<section class="market-snapshot-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        <div class="market-snapshot-grid">
                            <div class="snapshot-image">
                                <img src="<?php bloginfo("template_directory"); ?>/assets/img/sold.png" alt="">
                            </div>
                            <div class="snapshot-description">
                               <?php 
                               $new_list_result = $wpdb->get_results("SELECT * FROM wp_property where built_type = 'New Construction' ");
								$num_list_rows = count( $new_list_result );
                                ?>
                                <h3>New listings</h3>
                                <!--h5>Single family Homes</h5-->
                                <h1><?php echo number_format($num_list_rows);?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="market-snapshot-grid">
                            <div class="snapshot-image">
                                <img src="<?php bloginfo("template_directory"); ?>/assets/img/rent.png" alt="">
                            </div>
                            <div class="snapshot-description">
                                <?php 
                               $new_rent_result = $wpdb->get_results("SELECT * FROM wp_property where  property_type_id = '6'");
								$num_rent_rows = count( $new_rent_result );
                                ?>
                                <h3>Rent Listings</h3>
                                <!--h5>Single family Homes</h5-->
                                <h1><?php echo number_format($num_rent_rows);?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="market-snapshot-grid">
                            <div class="snapshot-image">
                                <img src="<?php bloginfo("template_directory"); ?>/assets/img/commercial.png" alt="">
                            </div>
                            <div class="snapshot-description">
                                 <?php 
                               $new_commercial_result = $wpdb->get_results("SELECT * FROM wp_property where  property_type_id = '8'");
								$num_commercial_rows = count( $new_commercial_result );
                                ?>
                                <h3>Commercial Listings</h3>
                                <!--h5>Single family Homes</h5-->
                                <h1><?php echo number_format($num_commercial_rows);?></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="col-md-12">
				<div class="market-watch-btn">
					<a href="<?php echo get_permalink(208);?>">view market watch</a>
				</div>
			</div>
        </div>
    </div>
</section>

<section class="explore-area-section">
    <div class="explore-area-section-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 pad-none">
                    <article class="animation-box">
                        <div class="pic">
						<?php $estate_img_result_wtr = $wpdb->get_results("SELECT * FROM files where viewURI !='' LIMIT 12,1");?>
                            <img src="<?php echo $estate_img_result_wtr[0]->viewURI; ?>">
                            <div class="overlay"></div>
                        </div>
                        <!--<div class="title">
                            <h3><span class="thin-style">Waterfront</span> Listing </h3>                        
                        </div>-->
                        <div class="add-title">
                            <h3><span class="thin-style">Waterfront</span> Listing </h3>                        
                        </div>
                        <div class="popup">
                            <div class="button-holder">
                                <a href="<?php echo get_permalink(103);?>" class="button gray">View All <span class="add-info">Waterfront Listings</span></a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-4 pad-none">
                    <article class="animation-box">
                        <div class="pic">
						<?php $estate_img_resultmrkt = $wpdb->get_results("SELECT * FROM files where viewURI !='' LIMIT 10,1");?>
                            <img src="<?php echo $estate_img_resultmrkt[0]->viewURI; ?>">
                            <div class="overlay"></div>
                        </div>
                        <!--<div class="title">
                            <h3><span class="thin-style">MARKET</span> WATCH</h3>                        
                        </div>-->
                        <div class="add-title">
                            <h3><span class="thin-style">MARKET</span> WATCH</h3>                        
                        </div>
                        <div class="popup">
                            <div class="button-holder">
                                <a href="<?php echo get_permalink(208);?>" class="button gray">READ THE LATEST <span class="add-info">MARKET WATCH</span></a>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-4 pad-none">
                    <article class="animation-box">
                        <div class="pic">
						<?php $estate_img_resultnew = $wpdb->get_results("SELECT * FROM files where viewURI !='' LIMIT 9,1");?>
                            <img src="<?php echo $estate_img_resultnew[0]->viewURI; ?>">
                            <div class="overlay"></div>
                        </div>
                        <!--<div class="title">
                            <h3><span class="thin-style">New</span> Listings</h3>                        
                        </div>-->
                        <div class="add-title">
                            <h3><span class="thin-style">New</span> Listings</h3>                        
                        </div>
                        <div class="popup">
                            <div class="button-holder">
                                <a href="<?php echo get_permalink(99);?>" class="button gray">View All <span class="add-info">New Listings</span></a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>           
    </div>
</section>

<section class="beautiful-home-image-section">
    <div class="green-overlay-section">
        <div class="pink-umbrella">
            <img src="<?php bloginfo("template_directory"); ?>/assets/img/pink-umbrella.png" alt="">
        </div>
        <div class="overlay-content">
            <h3>Beautiful Homes</h3>
            <p>Your dream house is just a step ahead</p>
        </div>
        <div class="yellow-umbrella">
            <img src="<?php bloginfo("template_directory"); ?>/assets/img/yellow-umbrella.png" alt="">
        </div>
    </div>
	<div class="bottom-image-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="row">
					  <?php $estate_img_result_miami = $wpdb->get_results("SELECT * FROM files where viewURI !='' LIMIT 7,1");?>
						<div class="bottom-image-right">
							<article class="animation-box">
								<div class="pic">
									<img src="<?php echo $estate_img_result_miami[0]->viewURI; ?>" alt="">
									<div class="overlay"></div>
								</div>
								<div class="title">
									<h3><span class="thin-style">Miami</span> Dade</h3>                        
								</div>
								<div class="popup">
									<div class="button-holder">
										<a href="<?php echo get_permalink(215);?>" class="button gray"> Visit Miami Dade County</a>
									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6">
					<div class="row">
					<?php $estate_img_resultbro = $wpdb->get_results("SELECT * FROM files where id ='MDIyOXwwMDAwMDAwMDA5Nnw5Ng..' and viewURI !=''");?>
						<div class="bottom-image-right">
							<article class="animation-box">
								<div class="pic">
									<img src="<?php echo $estate_img_resultbro[0]->viewURI; ?>" alt="">
									<div class="overlay"></div>
								</div>
								<div class="title">
									<h3><span class="thin-style">Broward</span> County</h3>                        
								</div>
								<div class="popup">
									<div class="button-holder">
										<a href="<?php echo get_permalink(219);?>" class="button gray">Visit Broward County</a>
									</div>
								</div>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--section class="footer-signup-section">
    <div class="container">
        <div class="col-xs-12">
            <div class="footer-signup-content">
                <h4>Everything you need for your new home. All in one place.</h4>
                <div class="footer-signup-btn">
                    <a data-toggle="modal" data-target="#modal-register" href="#">SIGN UP</a>
                </div>
            </div>
        </div>
    </div>
</section-->
