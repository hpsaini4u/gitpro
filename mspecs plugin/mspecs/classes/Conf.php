<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 05/02/15
 * Time: 17:47
 */

class Conf {

    public function __construct() {

    }

    /** set your own config parameters **/
    
    final public function get() {
        return array(
            'version' => '2.2.0',
            'encryptionKey' => "yourownencryptionkey",
            'internalToken' => "yourownencryptionkey",
            'mailer' => array(
                //email server config
                'server' => 'yoursmtpserver',
                'port' => '587',
                'username' => "youreplyaddress",
                'password' => "yourpassword",
                'charSet' => 'UTF-8',
                'SMTPSecure' => 'tls',
                'SMTPAuth' => true
            ),
            'useGoogleCaptcha' => true,
            'gooleCaptcha' => array(
                'secretKey' => 'secretyougotfromgoogle',
                'siteKey' => 'keyyougotfromgoogle'
            )
        );
    }

}
