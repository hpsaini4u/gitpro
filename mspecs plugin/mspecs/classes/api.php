<?php

class Mspecs_Api
{
    const DEBUG = true;

    protected $_apiUrl = null;
    protected $_cache = null;
    protected $_connection = null;
    protected $_forceCached = false;
    protected $_logFile = null;
    protected $_password = null;
    protected $_username = null;
    protected $_requestCall = false;
    protected $_noCacheCall = false;
    protected $_internalToken = null;
    protected $_config = null;

    public function __construct($apiurl, $username, $password)
    {
        $this->_config = MSPECS::getConfig()->get();
        $this->_internalToken = $this->_config['internalToken'];
        $this->_apiUrl = trim($apiurl, '/') . '/';
        $this->_username = $username;
        $this->_password = $password;

        $uploadDir = wp_upload_dir();
        $path = $uploadDir['basedir'] . '/cache';
        $url = $uploadDir['baseurl'] . '/cache';

        $this->_cache = MSPECS::getCache($path, $url);

        if (self::DEBUG) {
            $this->_logFile = $path . '/data/debug.log';
            if (!file_exists($this->_logFile)) {
                file_put_contents($this->_logFile, '');
            }
        }
    }

    public function getUser() {
        return $this->_username;
    }

    public function getPassword() {
        return $this->_password;
    }

    public function getUrl() {
        return $this->_apiUrl;
    }

    public function getInternalToken() {
        return $this->_internalToken;
    }

    public function getBrokers($args = array())
    {
        $brokers = array();
        $contacts = ($this->query(array(
            'path' => "search/systemUser?q=accessRights.name%20IN%20(%27ACCESS_RIGHT_BROKER%27)%20AND%20userAccounts.validUntil>%272016-10-17%27"
        )));
        if (is_array($contacts)) {
           foreach ($contacts as $contact) {
                $brokers[] = new Mspecs_Model_Contact($contact);
            }
        }

        $collection = new Mspecs_Collection($brokers, $args);
        return $collection;
    }

    public function getAccessRightId($accessRight = "ACCESS_RIGHT_BROKER") {
        if($data = ($this->queryOne(array(
                'path' => 'accessRights',
                'query' => array('q' => "name='{$accessRight}'")
            )))) {
            return $data;
        }
        return null;
    }

    public function getAssistants($args = array()) {
        $assistants = array();
        $contacts = ($this->query(array(
            'path' => "search/systemUser?q=accessRights.name%20IN%20(%27ACCESS_RIGHT_ASSISTANT%27)"
        )));
        if (is_array($contacts)) {
            foreach($contacts as $contact) {
                $assistants[] = new Mspecs_Model_Contact($contact);
            }
        }

        $collection = new Mspecs_Collection($assistants, $args);
        return $collection;
    }

    public function getUserAccount($id) {
        $data = ($this->queryOne(array(
            'path' => 'userAccounts',
            'query' => array('q' => "id='{$id}'"),
        )));
        if(is_array($data)) {
            return new Mspecs_Model($data);
        }
        return null;
    }

    public function getContact($id) {
        $data = ($this->queryOne(array(
            'path' => 'contacts',
            'query' => array('q' => "id='{$id}'"),
        )));
        if (is_array($data)) {
            return new Mspecs_Model_User($data);
        }
        return null;
    }

    public function getContactById($id)
    {
        $data = ($this->queryOne(array(
                    'path' => 'contacts',
                    'query' => array('q' => "id='{$id}'"),
                )));
        if (is_array($data)) {
            return new Mspecs_Model_Contact($data);
        }
        return null;
    }

    public function getCompany($args = array())
    {
        if ($data = $this->queryOne(array('path' => 'organizations'))) {
            return new Mspecs_Model_Company($data);
        }
        return null;
    }

    public function getCompanyById($companyId) {
        if($data = $this->queryOne(array(
            'path' => 'organizations',
            'query' => array('q'=>"id='{$companyId}'")
        ))) {
            if(is_array($data)) {
                return new Mspecs_Model_Company($data);
            }
        }
    }

    public function getPublicDealById($dealId) {
        $data = ($this->queryOne(array(
            'path' => 'deals',
            'query' => array(
                'q' => "id='{$dealId}' and isPublished=true"
            ),
        )));
        if (is_array($data)) {
            return new Mspecs_Model_Deal($data);
        }
        return null;
    }

    public function getDealById($dealId)
    {
        $data = ($this->queryOne(array(
                    'path' => 'deals',
                    'query' => array(
                        'q' => "id='{$dealId}'"
                    ),
                )));
        if (is_array($data)) {
            return new Mspecs_Model_Deal($data);
        }
        return null;
    }

    public function getBiddingById($biddingId) {
        $data = ($this->queryOne(array(
            'path' => 'biddings',
            'query' => array(
                'q' => "id='{$biddingId}'"
                ),
            )));
        if(is_array($data)) {
            return new Mspecs_Model_Bidding($data);
        }

        return null;
    }

    public function getHousingAsociationById($asosId) {
        $data = ($this->queryOne(array(
            'path' => 'housingAssociations',
            'query' => array(
                'q' => "id='{$asosId}'"
            ),
        )));

        if(is_array($data)) {
            return new Mspecs_Model_Housingassociation($data);
        }
        return null;
    }

    public function getDeals($args = array())
    {
        $args = array_merge($args, array('path' => 'deals'));
        if (!isset($args['limit']))
            $args['limit'] = 200;
        $data = $this->query($args);

        //WTF!!! - this is temporary measurement, will be replaced by sorting from shortcode
        if(!isset($args['sort'])) {
            $args = array_merge($args, array('sort' => '-updatedDate'));
        }

        $deals = array();
        if (is_array($data)) {
            foreach ($data as $dealData) {
                $deals[] = new Mspecs_Model_Deal($dealData);
            }
        }
        $collection = new Mspecs_Collection($deals, $args);
        return $collection;
    }

    public function ByMunicipalityName($args = array(), $geoAreaName)
    {
        $data = $this->getDeals($args);
        $deals = array();
        foreach($data as $dealData) {
            foreach($this->getGeoAreasByEstate($dealData->getMainEstateId()) as $geoArea)
            {
                if($geoArea['name'] == $geoAreaName && $geoArea['parentId']) {
                    $deals[] = $dealData;
                }
            }
        }
        $collection = new Mspecs_Collection($deals);
        return $collection;
    }

    public function getGeoAreasByEstate($estateId) {
        if($estateGeoAreas = $this->query(array(
            'path' => 'estateGeographicalAreas',
            'query' => array(
                'q' => "estateId='{$estateId}'"
            )
        ))) {
            $geoAreas = array();
            if(is_array($estateGeoAreas)) {
                foreach($estateGeoAreas as $estateGeo) {
                    if($geoArea = $this->queryOne(array(
                        'path' => 'geographicalAreas',
                        'query' => array(
                            'q' => "id='{$estateGeo['geographicalAreaId']}'"
                        )
                    ))) {
                        if(is_array($geoArea)) {
                            $geoAreas[] = $geoArea;
                        }
                    }
                }
                return $geoAreas;
            }
        };
        return null;
    }

    public function getEasementsAndDetailsByEstateId($estateId) {
      $collection = null;
      if ($data = $this->query(array(
        'path' => 'easementsAndDetails',
        'query' => array(
          'q' => "estateId='{$estateId}'"
        )
      ))) {
        $easetmentsList = array();
        if (is_array($data) && !empty($data)) {
          foreach ($data as $key => $value) {
            array_push($easetmentsList, new Mspecs_Model_Easement_And_Details($value));
          }
          $collection = new Mspecs_Collection($easetmentsList);
        }
      }
        return $collection;
    }

    public function getDealsByMunicipality($args = array(), $geoAreaName) {
        $args = array_merge($args, array('path' => 'deals'));
        $dealData = $this->query($args);
        return $this->getEstatesByDealsMunicipalityArray($dealData, $geoAreaName);
    }

    public function getEstatesByDealsMunicipalityArray($dealArray, $geoAreaName) {
        $deals = array();
        if(is_array($dealArray)) {
            $estateIds = array_map(function ($obj) { return $obj['mainEstateId']; }, $dealArray);
            $estatesGeos = ($this->query(array(
                'path' => 'estateGeographicalAreas',
                'query' => array(
                    'q' => "estateId IN ('" . join("','", $estateIds) . "')"
                )
            )));
            if(is_array($estatesGeos)) {
                $geoAreaIds = array_map(function ($obj) { return $obj['geographicalAreaId']; }, $estatesGeos);
                $geoAreas = ($this->query(array(
                    'path' => 'geographicalAreas',
                    'query' => array(
                        'q' => "id IN ('" . join("','", $geoAreaIds) . "')"
                    )
                )));

                foreach($geoAreas as $geo) {
                    if($geo['name'] == $geoAreaName && $geo['parentId']) {
                        //retrieve deal
                        foreach($estatesGeos as $estateGeo) {
                            if(array_search($geo['id'], $estateGeo)) {
                                foreach($dealArray as $deal) {
                                    if($deal['mainEstateId'] == $estateGeo['estateId']) {
                                        $deals[] = new Mspecs_Model_Deal($deal);
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        $collection = new Mspecs_Collection($deals);
        return $collection;
    }

    public function getDealEstatesByObjectAndMunicipality($args = array(), $objectType, $geoAreaName)
    {
        $objectTypeId = $this->getObjectTypeId($objectType);
        $args = array_merge($args, array('path' => 'deals'));
        $args['query'] .= " AND objectTypeId='" . $objectTypeId . "'";

        $dealData = $this->query($args);
        return $this->getEstatesByDealsMunicipalityArray($dealData, $geoAreaName);
    }

    public function getEstatesByObject($objectType, $args = array())
    {
        $objectTypeId = $this->getObjectTypeId($objectType);
        $args['query'] .= "q=objectTypeId='{$objectTypeId}'";
        $args = array_merge($args, array('path' => 'estates'));
        $data = $this->query($args);
        $estates = array();
        if (is_array($data)) {
            foreach ($data as $estateData) {
                $estates[] = new Mspecs_Model_Estate($estateData, $args);
            }
        }
        $collection = new Mspecs_Collection($estates);
        return $collection;
    }

    public function getDealsByObject($objectType, $args)
    {

        $objectTypeId = $this->getObjectTypeId($objectType);

        if($args['query'] != '') {
            $args['query'] .= " and objectTypeId='{$objectTypeId}'";
        } else {
            $args['query'] = "objectTypeId='{$objectTypeId}'";
        }

        $args = array_merge($args, array('path' => 'deals'));
        $data = $this->query($args);

        $deals = array();
        if (is_array($data)) {
            foreach ($data as $dealData) {
                $deals[] = new Mspecs_Model_Deal($dealData, $args);
            }
        }
        $collection = new Mspecs_Collection($deals);
        return $collection;
    }

    public function getEstateById($id)
    {
        $data = ($this->queryOne(array(
                    'path' => 'estates',
                    'query' => array(
                        'q' => "id='{$id}'"
                    ),
                )));
        if (is_array($data)) {
            return new Mspecs_Model_Estate($data);
        }
        return null;
    }

    public function getEstates($args = array())
    {
        $args = array_merge($args, array('path' => 'estates'));
        if (!isset($args['limit']))
            $args['limit'] = 100;
        $data = $this->query($args);

        $estates = array();
        if (is_array($data)) {
            foreach ($data as $estateData) {
                $estates[] = new Mspecs_Model_Estate($estateData);
            }
        }
        $collection = new Mspecs_Collection($estates, $args);
        return $collection;
    }

    public function getOtherOperatingCosts($estateId) {
        $data = ($this->query(array(
            'path' => 'estateOperatingCosts',
            'query' => array(
                'q' => "estateId='{$estateId}'"
            ),
        )));

        if(is_array($data)) {
            $operatingCosts = array();
            foreach($data as $operatingCost) {
                $operatingCosts[] = new Mspecs_Model_OperatingCost($operatingCost);
            }
            $collection = new Mspecs_Collection($operatingCosts);
            return $collection;
        }
        return null;
    }

    public function getPatios($estateId) {
        $data = ($this->query(array(
            'path' => 'patios',
            'query' => array(
                'q' => "estateId='{$estateId}'"
            ),
        )));

        if(is_array($data) && !empty($data)) {
            $patios = array();
            foreach($data as $patio) {
                $patios[] = new Mspecs_Model_Patio($patio);
            }
            $collection = new Mspecs_Collection($patios);
            return $collection;
        }
        return null;
    }

    public function getParkingSpaces($estateId) {
        $data = ($this->query(array(
            'path' => 'estateParkingSpaces',
            'query' => array(
                'q' => "estateId='{$estateId}'"
            ),
        )));

        $ids = "";
        $parkingSpaces = array();

        if(is_array($data) && !empty($data)) {
            $last_key = key( array_slice( $data, -1, 1, TRUE ) );
            foreach($data as $key => $estateParking) {
                $ids .= "'{$estateParking['parkingSpaceId']}'";
                if ($last_key != $key) $ids .= ", ";
            }
            if($ids != "") {
                $p_data = ($this->query(array(
                    'path' => 'parkingSpaces',
                    'query' => array(
                        'q' => "id IN (" . $ids . ")"
                    ),
                )));

                if(is_array($p_data)) {
                    foreach($p_data as $parking) {
                        $parkingSpaces[] = new Mspecs_Model_ParkingSpace($parking);
                    }
                }
            }
        }
        $collection = new Mspecs_Collection($parkingSpaces);
        return $collection;
    }

    public function getFileById($id, $publishedOnly = true, $tags = array())
    {
        if($id) {
            $q = "id='{$id}'";
            if ($publishedOnly)
                $q .= " and isPublished=true";

            $query = array(
                'path' => 'files',
                'query' => array(
                    'q' => $q
                ),
            );

            if (($data = $this->queryOne($query)) && is_array($data)) {
                return new Mspecs_Model_File($data);
            }
        }

        return null;
    }

    public function getFileCategory($id)
    {
        if ($data = ($this->queryOne(array(
                    'path' => 'files',
                    'query' => array('q' => "id='{$id}'")
                )))) {
            if (isset($data['fileCategoryId']) && ($category = ($this->queryOne(array(
                        'path' => 'fileCategories',
                        'query' => array('q' => "id='{$data['fileCategoryId']}'")
                    ))))) {
                return $category['name'];
            }
        }
        return null;
    }

    public function getObjectTypeId($objectType)
    {
        $data = ($this->queryOne(array(
                    'path' => 'objectTypes',
                    'query' => array(
                        'q' => "name='{$objectType}'"
                    ),
                )));
        if (is_array($data)) {
            return $data['id'];
        }
        return null;
    }

    public function getObjectType($id)
    {
        $data = ($this->queryOne(array(
            'path' => 'objectTypes',
            'query' => array(
                'q' => "id='{$id}'"
            ),
        )));
        if (is_array($data)) {
            return $data['name'];
        }
        return null;
    }

    public function getObjectSubType($id) {
      $data = ($this->queryOne(array(
          'path' => 'objectSubTypes',
          'query' => array(
              'q' => "id='{$id}'"
          ),
      )));
      if (is_array($data)) {
          return $data['name'];
      }
      return null;
    }

    public function getFarmingMainPurposes($estateId) {
        $farmings = array();
        if($data = ($this->query(array(
            'path' => 'estateFarmingPurposes',
            'query' => array(
                'q' => "estateId='{$estateId}'"),
        )))) {
            //retrieve all ids of farming
            $farmingPurposesIds = array();
            if(!empty($data)) {
                $farmingPurposesIds = array_map(function ($obj) { return $obj['farmingPurposeId']; }, $data);
            }
            if($farmingPurposes = $this->query(array(
                'path' => 'farmingPurposes',
                'query' => array(
                    'q' => "id IN ('" . join("','", $farmingPurposesIds) . "')"
                )))) {
                if(is_array($farmingPurposes) && !empty($farmingPurposes))
                    foreach($farmingPurposes as $farming) {
                        $farming[] = new Mspecs_Model_FarmingPurposes($farming);
                    }
            }

            $collection = new Mspecs_Collection($farmings);
            return $collection;
        }
    }

    public function getBuildings($estateId, $objectType = 'OBJECT_TYPE_FARMING') {
        $q = "estateId='{$estateId}'";
        if ($objectType == 'OBJECT_TYPE_COMMERCIAL') {
            $q .= ' AND isCommercialBuilding = true';
        }
        $data = ($this->query(array(
            'path' => 'buildings',
            'query' => array(
                'q' => $q),
                'sort' => 'displayOrder'
        )));
        $buildings = array();

        if(is_array($data)) {
            foreach($data as $building) {
                array_push($buildings, new Mspecs_Model_Building($building));
            }
            $collection = new Mspecs_Collection($buildings);
            return $collection;
        }
        return null;
    }

    public function getValuationUnits($estateId) {
        $data = ($this->query(array(
            'path' => 'buildings',
            'query' => array(
                'q' => "estateId='{$estateId}' AND isCommercialBuilding = false",
                'sort' => '-displayOrder'
            )
        )));
        $valuationUnits = array();
        if (is_array($data)) {
            foreach($data as $valuationUnit) {
                array_push($valuationUnits, new Mspecs_Model_Building($valuationUnit));
            }
            $collection = new Mspecs_Collection($valuationUnits);
            return $collection;
        }
        return null;
    }

    public function getAreas($estateId) {
        $data = ($this->query(array(
            'path' => 'areas',
            'query' => array(
                'q' => "estateId='{$estateId}' and isPublished=true"),
        )));
        $areas = array();
        if(is_array($data)) {
            foreach($data as $area) {
                $areas[$area['displayOrder']-1] = new Mspecs_Model_Area($area);
            }
        }
        if(sizeof($areas)>0) {
            ksort($areas);
            $collection = new Mspecs_Collection($areas);
            return $collection;
        }
        return null;
    }

    public function getViewingById($viewingId) {
        $data = ($this->queryOne(array(
            'path' => 'viewings',
            'query' => array(
                'q' => "id='{$viewingId}'"
            )
        )));
        if(is_array($data)) {
            return new Mspecs_Model_Visit($data);
        }
        return null;
    }

    public function getRoomsByEstate($estateId)
    {
        $data = ($this->query(array(
                    'path' => 'rooms',
                    'query' => array('q' => "estateId='{$estateId}'"),
                    'sort' => 'displayOrder'
                )));
        $rooms = array();
        if (is_array($data)) {
            foreach ($data as $roomData) {
                $rooms[] = new Mspecs_Model_Room($roomData);
            }
        }
        $collection = new Mspecs_Collection($rooms);
        return $collection;
    }

    public function getUrls($args = array())
    {
        $args = array_merge($args, array('path' => 'dealUrls'));
        $data = $this->query($args);
        $urls = array();
        if (is_array($data)) {
            foreach ($data as $urlData) {
                $urls[] = new Mspecs_Model_Url($urlData);
            }
        }
        $collection = new Mspecs_Collection($urls, $args);
        return $collection;
    }

    public function getUrlsByDealId($query)
    {
        $urls = array();
        if ($data = $this->query($query)) {
            foreach ($data as $urlData) {
                $urls[] = new Mspecs_Model_Url($urlData);
            }
        }
        $collection = new Mspecs_Collection($urls);
        return $collection;
    }

    public function getUserColors()
    {
        $data = $this->query(array(
            'path' => 'webhome',
            'query' => array('q' => "header like 'WEBHOME_HEADER_COLOR_%'")
                ));

        $colors = array();
        if(isset($data)) {
                foreach ($data as $color) {
                $header = str_replace('WEBHOME_HEADER_COLOR_', '', $color["header"]);
                $colors[$header] = $color["body"];
            }
        }
        return $colors;
    }

    public function getWebhome($args = array())
    {
        $args = array_merge($args, array('path' => 'webhome'));
        $data = $this->queryOne($args);
        if (is_array($data)) {
            return new Mspecs_Model_Webhome($data);
        }
        return null;
    }

    public function getHousingAssociation($id) {

        if($housingData = $this->queryOne(array(
            'path' => 'housingAssociations',
            'query' => array(
                'q' => "id='{$id}'"
            ),
        ))) {
            return new Mspecs_Model_Housingassociation($housingData);
        }
        return null;
    }

    public function getFarmingSpecifics($estateId) {

        $data = ($this->query(array(
            'path' => 'farmingSpecifics',
            'query' => array('q' => "estateId='{$estateId}'"),
        )));
        $farmingSpecifics = array();
        if (is_array($data)) {
            foreach ($data as $farmingData) {
                $farmingSpecifics[$farmingData['displayOrder']] = new Mspecs_Model_FarmingSpecifics($farmingData);
            }
        }
        if(sizeof($farmingSpecifics)>0) {
            ksort($farmingSpecifics);
            $collection = new Mspecs_Collection($farmingSpecifics);
            return $collection;
        }
        return null;
    }

    public function getWebsiteOption($option) {
        $data = ($this->queryOne(array(
            'path' => 'webhome',
            'query' => array(
                'q' => "header='{$option}'"
            ),
        )));
        if(is_array($data)) {
            return new Mspecs_Model_Webhome($data);
        }
        return null;
    }

    /**
     * Check if there is next page available
     *
     * @param mixed string or array of url query
     * @return mixed false or array of next url query
     */
    public function hasNextPage($url)
    {
        if (is_string($url))
            $url = $this->_parseUrl($url);

        try {

            if (!is_array($url))
                throw new Exception('Url options are not set');

            $paths = array(
                'getDeals' => 'deals',
                'getEstates' => 'estates'
            );
            if (isset($url['api']) && isset($paths[$url['api']])) {
                $url['path'] = $paths[$url['api']];
            }

            if (!isset($url['path']))
                throw new Exception('Url path is not specified');

            $limit = @intval($url['limit']);
            if (!$limit)
                throw new Exception('Limit is not set');

            $offset = @intval($url['offset']);
            if (!$offset)
                $offset = 0;

            $url['offset'] = $offset + $limit;

            if (($data = $this->query($url)) && sizeof($data) > 0) {
                return true;
            }
        } catch (Exception $e) {

        }
        return false;
    }

    public function query($url)
    {
        $data = $this->_query($this->_prepareUrl($url));
        return $data;
    }

    public function queryOne($url)
    {
        if (is_string($url))
            $url = $this->_parseUrl($url);
        $data = $this->query(array_merge($url, array('limit' => 1)));
        if (is_array($data) && isset($data[0])) {
            return $data[0];
        }
        return null;
    }

    public function checkToken($token)
    {
        // todo: check token from MSPECS api
        return true;
    }

    public function log($data)
    {
        if (self::DEBUG && $this->_logFile) {
            if (!is_string($data)) {
                $data = json_encode($data);
            }
            $file = fopen($this->_logFile, 'a');
            fwrite($file, date('YmdHi: ') . $data . "\n");
            fclose($file);
        }
    }

    public function setForceCached($force)
    {
        $this->_forceCached = $force;
    }

    public function setCallRequest($call) {
       $this->_requestCall = $call;
    }

    public function setNoCacheCall($call) {
        $this->_noCacheCall = $call;
    }

    public function getForceCached() {
        return $this->_forceCached;
    }

    public function _getConnection()
    {
        if (!$this->_connection) {
            $connection = curl_init();
            curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false); // disable SSL verification
            // curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($connection, CURLOPT_USERPWD, $this->_username . ":" . $this->_password);
            curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);  // return response
            curl_setopt($connection, CURLOPT_COOKIESESSION, true);
            $this->_connection = $connection;
        }
        return $this->_connection;
    }

    protected function getVersion($url) {
            $version = MSPECS::getSysConf()->get()['version'];
            if(empty($version)) {
                $version = 'noversion';
            }

            if (strpos($url, '?') == false) {
                return '?wp=' . $version;
            } else {
                return '&wp=' . $version;
            }
    }

    public function _getTags($queryUrl, $queryData)
    {
        $params = $this->_parseUrl($queryUrl);
        $tags = array($params['path']);
        $url = $this->_apiUrl . "schema/" . $params['path'];

        if ((isset($params['limit']) && $params['limit'] > 1) || isset($params['offset']) || sizeof($queryData) > 1) {
            $tags[] = "{$params['path']} list";
        }

        try {
            $hash = md5($url);
            if (!($data = $this->_cache->get($hash))) {
                $connection = $this->_getConnection();

                curl_setopt($connection, CURLOPT_URL, $url . $this->getVersion($url));
                $data = json_decode(curl_exec($connection), true);
                if (!$data) {
                    $this->log("Schema for {$params['path']} is not available");
                    throw new Exception('Data cannot be parsed');
                }

                $this->_cache->set($hash, $data, array("schema", "{$params['path']} schema"));
            }

            $idFields = array();
            if (is_array($data) && is_array(@$data['publicFields'])) {
                foreach ($data['publicFields'] as $field) {
                    if (isset($field['name']) && in_array(@$field['type'], array('primaryKey', 'foreignKey'))) {
                        $idFields[] = preg_quote(trim($field['name']));
                    }
                }
            }

            $objectIds = array();
            if (preg_match_all('/"(' . implode('|', $idFields) . ')":"([A-z0-9_.-]+)"/', json_encode($queryData), $objectIds)) {
                $tags = array_merge($tags, array_unique($objectIds[2]));
            }
        } catch (Exception $e) {
        }

        return $tags;
    }

    /**
     * Parse URL for options
     * @param string $url
     * @return array options
     */
    protected function _parseUrl($url)
    {
        $options = array();
        $url = parse_url($url);

        if (!empty($url['path']))
            $options['path'] = ltrim($url['path'], '/');
        if (!empty($url['query'])) {
            $params = array();
            parse_str($url['query'], $params);
            $options['query'] = $params;

            foreach (array('limit', 'offset', 'sort') as $param) {
                if (isset($params[$param])) {
                    $options[$param] = $params[$param];
                    unset($options['query'][$param]);
                }
            }
        }
        return $options;
    }

    /**
     * Prepare url with escaping characters
     * Options may be a string or an array of:
     *  - path   {string} estates, deals, etc...
     *  - query  {mixed}  string or array(param => value)
     *  - sort   {string}
     *  - limit  {int}
     *  - offset {int}
     *
     * @param mixed $options url string or array of options
     * @return string url
     */
    protected function _prepareUrl($options)
    {
        if (is_string($options)) {
            $options = $this->_parseUrl($options);
        }

        $delimiter = '?';

        if (is_array($options)) {
            $url = '';
            if (!empty($options['path'])) {
                $url .= ltrim($options['path'], '/');
                $delimiter = (strpos($url, '?') !== false) ? '&' : '?';
            }
            if (!empty($options['query'])) {
                if (is_string($options['query'])) {
                    //$url .= $delimiter . 'q=' . ltrim(urlencode($options['query']), '?');
                    $params = array();
                    parse_str($options['query'], $params);
                    $options['query'] = $params;
                }
                if (is_array($options['query'])) {
                    $params = array();
                    foreach ($options['query'] as $param => $value) {
                        if (is_array($value)) {
                            foreach ($value as $v) {
                                $params[] = $param . '[]=' . urlencode($v);
                            }
                        } else {
                            $params[] = $param . '=' . urlencode($value);
                        }
                    }
                    if (sizeof($params) > 0) {
                        $url .= $delimiter . implode('&', $params);
                    }
                }
                $delimiter = (strpos($url, '?') !== false) ? '&' : '?';
            }
            if (!empty($options['limit']) && intval($options['limit']) > 0) {
                $url .= $delimiter . 'limit=' . intval($options['limit']);
                $delimiter = '&';
            }
            if (!empty($options['offset']) && intval($options['offset']) > 0) {
                $url .= $delimiter . 'offset=' . intval($options['offset']);
                $delimiter = '&';
            }
            if (!empty($options['sort'])) {
                $url .= $delimiter . 'sort=' . urlencode($options['sort']);
                $delimiter = '&';
            }
            return $url;
        }

        return null;
    }

    /***
     * PLEASE DO NOT CHANGE ANY CODE BELOW
     */
    protected function _query($url)
    {
        if (!$url) {
            throw new Exception('URL is not specified');
        }

        $url = trim($url, '/');
        $hash = md5($this->_username . ':' . $url);
        $data = null;

        if(!$this->_noCacheCall) {
            $data = $this->_cache->get($hash, true);
            if ($this->_forceCached) {
                if(empty($data) && $this->_requestCall) {
                    $this->_cache->removeCacheByHash($hash);
                }
                return $data;
            }
        }

        if (!is_array($data)) {
            $connection = $this->_getConnection();
            curl_setopt($connection, CURLOPT_URL, $this->_apiUrl . $url . $this->getVersion($url));
            $this->log('query: ' . $url);
            $data = json_decode(curl_exec($connection), true);
            if(is_array($data)) {
                if (isset($data[0]['status']) && isset($data[0]['code']) && isset($data[0]['message'])) {
                    if (defined('WP_DEBUG') && WP_DEBUG) {
                        throw new Exception("Error {$data[0]['code']}: {$data[0]['message']}");
                    } else {
                        $data = null;
                    }
                } else {
                    $this->_cache->set($hash, $data, $this->_getTags($url, $data), array('url' => $url));
                }
            }
        }
        return $data;
    }
}
