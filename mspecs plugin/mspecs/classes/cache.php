<?php

/***
 * PLEASE DO NOT CHANGE ANY CODE BELOW
 */

class Mspecs_Cache
{

    protected $_dataPath = null;    // path to cache data files
    protected $_filesPath = null;   // path to cached files
    protected $_filesUrl = null;    // cache directory url
    protected $_lifetime = null;    // cache lifetime in seconds. set 86400 for 1 day cache
    protected $_time = null;        // current run time

    public function __construct($path = null, $url = null)
    {
        if (!$path) {
            $uploadDir = wp_upload_dir();
            $path = $uploadDir['basedir'] . '/cache';
            $url = $uploadDir['baseurl'] . '/cache';
        }

        $path = rtrim(str_replace(DIRECTORY_SEPARATOR, '/', $path), '/');

        if (!file_exists($path)) {
            @mkdir($path, 0777, true);
        }

        if (is_dir($path) && is_writable($path)) {
            $this->_dataPath = $path . '/data';
            $this->_filesPath = $path . '/files';
            $this->_filesUrl = $url . '/files';
            $this->_time = time();

            @mkdir($this->_dataPath, 0777, true);
            @mkdir($this->_filesPath, 0777, true);

            if (!file_exists($this->_dataPath . '/' . '.htaccess')) {
                file_put_contents($this->_dataPath . '/' . '.htaccess', 'deny from all');
            }
        }
    }

    public function get($hash, $forceCached = false)
    {
        if (($file = $this->_getCacheFilePath($hash)) && ($content = $this->_getCacheFileContent($file))) {
            if (is_array($content) && isset($content['data'])) {
                if ($forceCached || (!$this->_lifetime || ($this->_time - $this->_lifetime < $content['info']['modified']))) {
                    return $content['data'];
                }
            }
        }
        return null;
    }

    public function removeCacheByHash($hash) {
        if($file = $this->_getCacheFilePath($hash)) {
            if(file_exists($file)) {
                @unlink($file);
            }
        }
    }

    public function set($hash, $data, $tags = array(), $info = array())
    {
        if ($file = $this->_getCacheFilePath($hash)) {
            if (!is_dir(dirname($file))) {
                @mkdir(dirname($file), 0777, true);
            }

            if (!is_array($tags))
                $tags = array();
            if (!is_array($info))
                $info = array();
            $info['modified'] = $this->_time;

            file_put_contents($file, json_encode(array(
                'data' => $data,
                'info' => $info,
                'tags' => $tags,
            )));
        }
    }

    /**
     * Load file from remote server and save it locally in cache
     *
     * @param string $url
     * @return string url in uploads
     */
    public function getFileUrl($url, $reload = false)
    {
        $filename = preg_replace('#^https?://[^/]+/(files/)?#i', '', $url);

        if (!$filename)
            throw new Exception("File is not defined in url '{$url}'");

        $filepath = $this->_filesPath . '/' . $filename;
        $fileurl = $this->_filesUrl . '/' . $filename;

        if ($reload && file_exists($filepath)) {
            @unlink($filepath);
        }

        if (!file_exists($filepath)) {
            stream_context_set_default(array(
                'http' => array(
                    'method' => "GET",
                    'header' => "Accept-language: en\r\n" .
                    "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:13.0) Gecko/20100101 Firefox/28.0.1\r\n"
                )
            ));

            if (($headers = @get_headers($url, 1)) && preg_match('/200/', $headers[0])) {
                @mkdir(dirname($filepath), 0777, true);
                if ($content = @file_get_contents($url)) {
                    MSPECS::getApi()->log('load file ' . $url);
                    file_put_contents($filepath, $content);
                }
            }
        }
        return $fileurl;
    }

    public function clearCache()
    {
        if ($this->_dataPath) {
            $this->_clearDirectory($this->_dataPath);
        }
    }

    public function clearFiles()
    {
        if ($this->_filesPath) {
            $this->_clearDirectory($this->_filesPath);
        }
    }

    /**
     * Update cache by tags. All tags should match to update cache
     * If callback function is not set, file will be removed
     *
     * @param array $tags
     * @param array $callback func($path, $content)
     */
    public function updateCacheByTags($tags, $callback = null, $matchMode = 'all')
    {
        if (is_string($tags))
            $tags = array($tags);
        if (!is_array($tags) || sizeof($tags) == 0)
            throw new Exception('Cache tags are not defined');

        if (!in_array($matchMode, array('all', 'any'))) {
            $matchMode = 'all';
        }

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->_dataPath));
        foreach ($files as $path => $file) {
            if ($file->isFile() && substr($path, -6) === '.cache' && ($content = $this->_getCacheFileContent($path))
                    && is_array($content) && isset($content['data'])
                    && @$content['info']['modified'] < $this->_time
                    && is_array(@$content['tags'])
                    && (($matchMode == 'all' && !array_diff($tags, $content['tags']))
                    || ($matchMode == 'any' && array_intersect($tags, $content['tags'])))
            ) {
                if ($callback && is_callable($callback)) {
                    call_user_func_array($callback, array($path, $content));
                } else {
                    @unlink($path);
                }
            }
        }
    }

    protected function _clearDirectory($path)
    {
        if (is_dir($path)) {
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path));
            foreach ($files as $filepath => $file) {
                if ($file->isFile()) {
                    @unlink($filepath);
                }
            }
        }
    }

    /**
     *
     * @param string $file path
     * @return mixed json_decoded value; null if file does not exist or cannot be decoded as json
     */
    protected function _getCacheFileContent($file)
    {
        if (is_file($file)) {
            return json_decode(file_get_contents($file), true);
        }
        return null;
    }

    public function _getCacheFilePath($hash)
    {
        if (!$this->_dataPath)
            throw new Exception('Cache data path is not set');
        if (!is_string($hash))
            throw new Exception('Cache data hash is incorrect');
        if (strlen($hash) < 32) {
            echo "I am here";
            return null;
        }
        $hash = strtolower($hash);
        $path = "{$this->_dataPath}/{$hash[0]}/{$hash[1]}/{$hash}.cache";
        return $path;
    }

}