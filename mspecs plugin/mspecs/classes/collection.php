<?php

/***
 * PLEASE DO NOT CHANGE ANY CODE BELOW
 */

MSPECS::includeFile('classes/model.php');

class Mspecs_Collection extends Mspecs_Model implements ArrayAccess, Countable, IteratorAggregate
{

    protected $_args = null;
    protected $_items = array();

    public function __construct($data, $args = null)
    {
        if (is_array($data)) {
            $this->_items = $data;
        } else {
            $this->_items[] = $data;
        }
        $this->_args = $args;
    }

    public function count()
    {
        return count($this->_items);
    }

    /**
     * Implementation of IteratorAggregate::getIterator()
     */
    public function getIterator()
    {
        return new ArrayIterator($this->_items);
    }

    public function offsetExists($offset)
    {
        return isset($this->_items[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->_items[$offset]) ? $this->_items[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->_items[] = $value;
        } else {
            $this->_items[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->_items[$offset]);
    }

    public function getItems()
    {
        return $this->_items;
    }

    public function setItems($items)
    {
        if (is_array($items)) {
            $this->_items = $items;
        }
        return $this;
    }

    public function hasNextPage()
    {
        return MSPECS::getApi()->hasNextPage($this->_args);
    }

    public function getNextPageUrl()
    {
        if ($this->hasNextPage()) {
            $args = $this->_args;
            if (!isset($args['offset']) || !intval($args['offset']))
                $args['offset'] = 0;
            $args['offset'] += $args['limit'];
            $url = MSPECS::getHelper()->getAjaxUrl($args);
            return $url;
        }
        return null;
    }

}