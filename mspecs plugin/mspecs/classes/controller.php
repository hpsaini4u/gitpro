<?php

/***
 * PLEASE DO NOT CHANGE ANY CODE BELOW
 */

class Mspecs_Controller
{

    protected $_template = null;
    protected $_templateVars = array();

    protected function _getApi()
    {
        return MSPECS::getApi();
    }

    protected function _getPost()
    {
        return MSPECS::getPost();
    }

    protected function _renderView($view = null, $vars = array())
    {
        if (is_null($view)) {
            $view = $this->_template;
        }
        $file = MSPECS::getViewPath($view);

        if (!file_exists($file)) {
            throw new Exception("Template file '{$view}' is not found");
        }

        ob_start();
        extract(array_merge($this->_templateVars, $vars));
        include($file);
        $content = ob_get_clean();
        return $content;
    }

}