<?php

/***
 * PLEASE DO NOT CHANGE ANY CODE BELOW
 */

class Mspecs_Helper
{

    protected $_page = null;
    //TO-DO make config file and read keys from there
    private $cryptoGuard = null;
    private $config = null;

    public function __construct() {
        $this->config = MSPECS::getConfig()->get();
    }

    public function getAjaxUrl($args = null)
    {
        if (!empty($args) && is_array($args)) {
            $params = array('action=mspecsapi');
            foreach ($args as $key => $value)
                $params[] = $key . '=' . urlencode($value);
            $url = admin_url('admin-ajax.php') . '?' . implode('&', $params);
            return $url;
        }
        return null;
    }

    public function getPage()
    {
        if (!$this->_page && ($pageId = MSPECS::getOption('api-page'))) {
            $this->_page = get_post($pageId);
        }
        return $this->_page;
    }

    public function getUrl($args = array())
    {
        if ($page = $this->getPage()) {

            $url = get_post_permalink($page->ID);

            if (is_array($args) && sizeof($args) > 0) {
                $params = array();
                foreach ($args as $key => $value)
                    $params[] = $key . '=' . urlencode($value);

                $delimiter = (strpos($url, '?') !== false) ? '&' : '?';
                $url .= $delimiter . implode('&', $params);
            }
            return $url;
        }
        return null;
    }

    public function printJson($object) {
        echo "<pre>" . json_encode($object, JSON_PRETTY_PRINT) . "</pre>";
    }

    //generate token
    public function generateToken($args) {
        //adding customer argument
        //push arguments into token
        $token = time() + (24 * (60 * 60 * 1000));
        foreach($args as $arg) {
            $token .= "|" . $arg;
        }
        //somewhere here add dealId and customer arguments
        return $this->encrypt($token);
    }

    public function validateToken($encrypted_string) {
        $pure_string = $this->decrypt($encrypted_string);
        $token = explode("|", $pure_string);
        $received_epoch = $token[0];
        //checks received8 epoch with current time
        if($received_epoch > time()) {
            return $token;
        }
        return false;
    }

    private function encrypt($pure_string) {
        $this->cryptoGuard = new \Coreproc\CryptoGuard\CryptoGuard($this->config['encryptionKey']);
        return $this->cryptoGuard->encrypt($pure_string);
    }

    private function decrypt($encrypted_string) {
        $this->cryptoGuard = new \Coreproc\CryptoGuard\CryptoGuard($this->config['encryptionKey']);
        return $this->cryptoGuard->decrypt($encrypted_string);
    }

    public function setMspecsAccount($username = null, $password = null, $url = null) {

    }

}
