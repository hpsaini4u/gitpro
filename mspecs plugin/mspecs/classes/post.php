<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 26/02/15
 * Time: 16:19
 */

class Mspecs_Post {

    protected $_connection = null;

    public function __construct() {
        $this->_config = MSPECS::getConfig();
    }

    public function _getConnection() {
        if (!$this->_connection) {
            $connection = curl_init();
            $api = MSPECS::getApi();
            curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false); // disable SSL verification
            curl_setopt($connection, CURLOPT_USERPWD, $api->getUser() . ":" . $api->getPassword());
            curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);  // return response
            curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($connection, CURLOPT_COOKIESESSION, true);
            $this->_connection = $connection;
        }
        return $this->_connection;
    }

    public function post($fields = array(), $url) {
        $fields_string = "";
        $connection = $this->_getConnection();
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');

        curl_setopt($connection,CURLOPT_URL, $url);
        curl_setopt($connection,CURLOPT_POST, count($fields));
        curl_setopt($connection,CURLOPT_POSTFIELDS, $fields_string);

        //execute post
        return json_decode(curl_exec($connection), true);
    }
}