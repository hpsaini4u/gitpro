/*! Mspecs plugin - v3.0.0
 * www.mspecs.se
 * Copyright (c) 2015; * Licensed GPLv2+ */

(function($){
    $(document).ready(function(){

        //resize logotype
        $('#logoimg').load(function() {
            var width = $(this).width();
            var height = $(this).height();
            if((width/4)>height) {
                $(this).width(200);
            } else {
                $(this).width(135);
            }
        });

        //button load more
        $('body').on('click', '.mspecs_load-more', function(){

            var container = $(this);
            var event = container.attr('data-event');
            var url = $(this).attr('href');

            container.addClass('loading');

            $.ajax({
                "complete": function(){
                    container.removeClass('loading');
                },
                "data": container.data('params') || {},
                "dataType": "json",
                "success": function(data){
                    if(data.status == 'success' && data.content){
                        var content = $('<div>').html(data.content).insertBefore(container);
                        $(window).trigger(event || 'load-page', [content, url]);
                        content.children().unwrap();
                        container.remove();
                    } else if(data.status == 'error' && data.message){
                        alert(data.message);
                    } else {
                        console.log(data);
                    }
                },
                "type": "post",
                "url": url
            });
            return false;
        });

        //set block display for pre tag li s
        $("li").each(function() {
            if($(this).children('pre').length>0) {
                $(this).css('display', 'block');
            }
        });

        if($('li:has(> pre)')) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'inline-flex');
        }

        //bidding on deal page
        $('#bidding-active').click(function() {
            $('#bidding-history').slideToggle("fast");
            if($('#bidding-arrow').hasClass("arrow-up")) {
                $('#bidding-arrow').removeClass("arrow-up");
                $('#bidding-arrow').addClass("arrow-down");
            } else {
                $('#bidding-arrow').removeClass("arrow-down");
                $('#bidding-arrow').addClass("arrow-up");
            }
        });

        $(".bookingbtn").click(function() {
            var booking = $('#bookingForm');
            //get id of button
            var viewingId = $(this).attr('id');
            //get booking time title
            var title = $(this).attr('title');
            //add value to hidden element
            $("#viewing_id").val(viewingId);
            $('#viewing_date').val(title);
            $("#bookingTitle").html("Booking on " + title);
            //if form is opened and same button is clicked
            if($(this).hasClass('.master') && booking.hasClass('.active')) {
                //close the form
                $(this).removeClass('.master');
                booking.removeClass('.active');
                booking.slideToggle("fast");
            } else if(!$(this).hasClass('.master') && booking.hasClass('.active')) {
                //it is not master but booking is open, so replace a master and data for booking
                //do not close the form
                $(".bookingbtn").removeClass(".master");
                $(this).addClass('.master');
            }  else {
                //non of them is master and booking is not open
                $(this).addClass('.master');
                $("#bookingForm").addClass('.active');
                booking.slideToggle("fast");
            }
        });

        //search
        var searchbox = $('#searchbox');
        searchbox.change(function(e) {
            var searchvalue = $(this).val();
            //search in every estate
            $(".estate").each(function() {
                var propertyname = $(this).find('.property-name').text();
                var propertycity = $(this).find('.property-city').text();

                if(propertyname.match(searchvalue) || propertycity.match(searchvalue)) {
                    $(this).show(500);
                } else {
                    $(this).hide(500);
                }
            });
        });

    });
})(jQuery);


