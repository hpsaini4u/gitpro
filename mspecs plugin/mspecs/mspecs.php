<?php

/*
  Plugin Name: MSPECS API
  Description: Plugin to display data from MSPECS
  Version: 2.2.0
  Author: MSPECS AB
  Author Email: info@mspecs.se
 */

if (!defined('ABSPATH')) {
    die();
}

class MSPECS
{

    protected static $_api = null;
    protected static $_post = null;
    protected static $_cache = null;
    protected static $_helper = null;
    protected static $_config = null;
    protected static $_sysconfig = null;
    protected static $_table = 'clientUsers';
    protected static $_db_version = '1.0';

    public static function getApi()
    {
        if (!self::$_api) {
            self::initDBcheck();
            //get api parameters form DB
            $apiLink = self::getParam('link');
            $username = self::getParam('username');
            $password = self::getParam('password');

            if (empty($apiLink) || empty($username) || empty($password)) {
                throw new Exception("MSPECS API credentials are not set");
            }

            self::$_api = new Mspecs_Api($apiLink, $username, $password);
        }
        return self::$_api;
    }

    public static function getPost() {
        if(!self::$_post) {
            self::$_post = new Mspecs_Post();
        }
        return self::$_post;
    }

    public static function getConfig() {
        if(!self::$_config) {
            self::$_config = new Conf();
        }
        return self::$_config;
    }

    public static function getSysConf() {
        if(!self::$_sysconfig) {
            self::$_sysconfig = new SysConf();
        }

        return self::$_sysconfig;
    }

    public static function getCache()
    {
        if (!self::$_cache) {
            self::$_cache = new Mspecs_Cache();
        }
        return self::$_cache;
    }

    public static function getHelper()
    {
        if (!self::$_helper) {
            self::$_helper = new Mspecs_Helper();
        }
        return self::$_helper;
    }

    public static function getOption($option)
    {
        return get_option('mspecsapi:' . $option, '');
    }

    public static function getViewPath($view)
    {
        return plugin_dir_path(__FILE__) . 'mvc/views/' . $view . '.phtml';
    }

    public static function setOption($option, $value)
    {
        update_option('mspecsapi:' . $option, $value);
    }

    public static function getParam($option) {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$_table;
        $site_url = get_bloginfo('url');
        $row = $wpdb->get_row($wpdb->prepare("SELECT {$option} FROM {$table_name} WHERE site = %s", $site_url));
        if(!empty($row)) {
            return $row->{$option};
        }
        else {
            return null;
        }
    }

    public static function setParams($username, $password, $link) {
        global $wpdb;
        $siteurl = get_bloginfo('url');
        $table_name = $wpdb->prefix . self::$_table;
        $wpdb->update($table_name, array('username' => $username, 'password' => $password, 'link' => $link), array('site'=>$siteurl));

    }

    public static function setParam($key, $value) {
        global $wpdb;
        $site_url = get_bloginfo('url');
        $table_name = $wpdb->prefix . self::$_table;
        $wpdb->update($table_name, array($key => $value), array('site' => $site_url));
    }

    public static function register()
    {
        //plugin activation hooks
        register_activation_hook(__FILE__, __CLASS__ . '::activatePlugin');
        self::includeFile('vendor/autoload.php');

        if (is_admin()) {
            self::includeFile('mvc/controllers/ajax.php'); // ajax is frontend, but wp loads it as admin
            self::includeFile('mvc/controllers/booking.php'); // booking is frontend, but wp loads it as admin
            self::includeFile('mvc/controllers/viewing.php'); // viewing is frontend, but wp loads it as admin
            self::includeFile('mvc/controllers/mailer.php'); // mailer is frontend, but wp loads it as admin
            self::includeFile('mvc/controllers/admin/settings.php');
        } else {
            self::includeFile('mvc/controllers/frontend.php');
            self::includeFile('mvc/controllers/shortcode.php');
            add_action('wp_enqueue_scripts', __CLASS__ . '::registerScripts');
        }
    }

    public static function activatePlugin() {
        MSPECS::createDB();
        MSPECS::insertRowDB();
        MSPECS::insertPreviousData();
    }

    public static function createDB() {

        global $wpdb;
        $table_name = $wpdb->prefix . self::$_table;

        $charset_collate = '';
        if ( ! empty( $wpdb->charset ) ) {
            $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
        }

        if ( ! empty( $wpdb->collate ) ) {
            $charset_collate .= " COLLATE {$wpdb->collate}";
        }

        $sql = "CREATE TABLE $table_name (
        id mediumint(9) NOT NULL AUTO_INCREMENT,
        username varchar(100) DEFAULT '' NOT NULL,
        password varchar(50) DEFAULT '' NOT NULL,
        link text NOT NULL,
        site text NOT NULL,
        name text,
        UNIQUE KEY (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        //keep table version in options
        add_option( 'mspecs_db_version', self::$_db_version );
    }

    public static function initDBcheck() {
        //check if there is a row we need
        $row = MSPECS::getParam('site');
        if(empty($row)) {
            //there is no table, so let's migrate everything from previous version of plugin
            MSPECS::createDB();
            MSPECS::insertRowDB();
            MSPECS::insertPreviousData();
        }
    }

    public static function insertRowDB() {
        global $wpdb;
        $table_name = $wpdb->prefix . self::$_table;
        $siteurl = get_bloginfo('url');

        $wpdb->insert($table_name, array('username'=>'', 'password'=>'', 'link'=>'', 'site'=>$siteurl), array('%s', '%s', '%s', '%s'));
    }

    public static function insertPreviousData() {
        $apiLink = self::getOption('api-link');
        $username = self::getOption('username');
        $password = self::getOption('password');

        if(!empty($apiLink) && !empty($username) && !empty($password)) {
            MSPECS::setParam('username', $username);
            MSPECS::setParam('password', $password);
            MSPECS::setParam('link', $apiLink);
        }
    }

    public static function registerScripts()
    {
		wp_enqueue_style('mspecs:css', plugin_dir_url(__FILE__) . 'includes/styles.css');
		wp_enqueue_style('print:css', plugin_dir_url(__FILE__) . 'includes/print.css');
        wp_enqueue_style('user:css', plugin_dir_url(__FILE__) . 'includes/user_css.php?' . http_build_query(self::getUserColors()));
        wp_enqueue_style('roboto:css', 'http://fonts.googleapis.com/css?family=Roboto:400,300,500italic,700');
        wp_enqueue_script('mspecs:js', 'https://www.google.com/recaptcha/api.js');
		wp_enqueue_script('script', plugin_dir_url(__FILE__) . 'includes/mspecs.js', array('jquery'), 1.1, true);
        // wp_enqueue_script( 'script', '/wp-content/plugins/mspecs/includes/mspecs.js', array ( 'jquery' ), 1.1, true);
		wp_enqueue_style('lightbox:css', plugin_dir_url(__FILE__) . 'includes/lightbox.css');
		wp_enqueue_script('lightbox:js', plugin_dir_url(__FILE__) . 'includes/lightbox.min.js', array('jquery'));
    }

    public static function includeFile($path)
    {
        foreach (glob(plugin_dir_path(__FILE__) . $path) as $file) {
            if (is_file($file)) {
                include_once($file);
            }
        }
    }

    private static function getUserColors()
    {
        $api = self::getApi();
        return $api->getUserColors();
    }

}

MSPECS::includeFile('classes/*.php');
MSPECS::includeFile('mvc/models/*.php');

MSPECS::register();
