<?php

class Mspecs_Controllers_Admin_Settings extends Mspecs_Controller
{

    public function __construct()
    {
        add_action('admin_menu', array($this, 'registerAdminMenu'));
    }

    public function registerAdminMenu()
    {
        add_options_page('MSPECS API Settings', 'MSPECS API', 'administrator', 'mspecsapi', array($this, 'settingsMenu'));
    }

    public function settingsMenu()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        if (sizeof($_POST) > 0) {
            if (isset($_POST['clear-cache'])) {
                MSPECS::getCache()->clearCache();
            } elseif (isset($_POST['clear-files'])) {
                MSPECS::getCache()->clearFiles();
            } elseif (isset($_POST['settings'])) {
                MSPECS::setOption('username', !empty($_POST['username']) ? $_POST['username'] : '');
                MSPECS::setOption('password', !empty($_POST['password']) ? $_POST['password'] : '');
                MSPECS::setOption('api-link', !empty($_POST['api-link']) ? $_POST['api-link'] : '');
                MSPECS::setOption('api-page', !empty($_POST['api-page']) ? $_POST['api-page'] : '');

                //setting db params
                MSPECS::setParam('username', !empty($_POST['username']) ? $_POST['username'] : '');
                MSPECS::setParam('password', !empty($_POST['password']) ? $_POST['password'] : '');
                MSPECS::setParam('link', !empty($_POST['api-link']) ? $_POST['api-link'] : '');
            }
        }

        echo $this->_renderView('admin/settings', array(
            'pages' => get_pages(array('hierarchical' => 0, 'parent' => 0))
        ));
    }

}

$settingsController = new Mspecs_Controllers_Admin_Settings();