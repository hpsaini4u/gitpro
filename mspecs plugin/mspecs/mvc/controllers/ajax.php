<?php

/***
 * PLEASE DO NOT CHANGE ANY CODE BELOW
 */

class Mspecs_Controller_Ajax extends Mspecs_Controller
{

    protected $api = null;
    protected $content = null;
    protected $debugMsg = null;

    public function __construct()
    {
        add_action('wp_ajax_mspecsapi', array($this, 'render'));
        add_action('wp_ajax_nopriv_mspecsapi', array($this, 'render'));
    }

    public function cache($attr)
    {
        set_time_limit(0);

        $cache = MSPECS::getCache();

        if (empty($attr['token']))
            throw new Exception('Direct access denied');
        if (!$this->api->checkToken($attr['token']))
            throw new Exception('Token is incorrect');

        if (isset($attr['clear'])) {
            $content = null;
            switch ($attr['clear']) {
                case 'all':
                    $cache->clearCache();
                    $cache->clearFiles();
                    break;
                case 'data':
                    $content = $cache->clearCache();
                    break;
                case 'deal':
                    if (empty($attr['deal']))
                        throw new Exception('Deal is not set');
                    $content = $this->_updateDeal($attr['deal']);
                    break;
                case 'bidding':
                    if (empty($attr['bidding']))
                        throw new  Exception("Bidding is not set");
                    $content = $this->_updateBidding($attr['bidding']);
                    break;
                case 'file':
                    if (empty($attr['file']))
                        throw new Exception('File is not set');
                    if (!preg_match('#^https?://[^/]+/.+#i', $attr['file']))
                        throw new Exception('File URL is incorrect');
                    $content = $cache->getFileUrl($attr['file'], true);
                    break;
                case 'files':
                    $content = $cache->clearFiles();
                    break;
                case 'object':
                    if (empty($attr['object']))
                        throw new Exception('Object is not set');
                    //also update company tree
                    $this->_updateCompanyTree();
                    $content = $cache->updateCacheByTags($attr['object'], array($this, 'recache'));
                default:
            }
            return $content;
        }
        return '';
    }

    public function render()
    {
        $attr = $_REQUEST;
        $content = '';
        $vars = array();
        $useJson = true;
        $useTemplate = true;

        $this->api = $this->_getApi();
        $this->api->setForceCached(false);

        try {
            if (empty($attr['api']))
                throw new Exception("'api' attribute is not set");

            $apiCalls = array(
                'getBrokers' => 'brokers',
                'getCompany' => 'company',
                'getDeals' => 'deals',
                'getEstates' => 'estates',
                'getUrls' => 'urls',
                'getWebhome' => 'webhome',
            );
            switch ($attr['api']) {
                case 'cache':
                    $useJson = true;
                    $useTemplate = false;
                    $content = $this->cache($attr);
                    break;
                case 'getDealById':
                    if (empty($attr['query']))
                        throw new Exception("Deal ID is not set");
                    $vars['deal'] = $this->api->getDealById($attr['query']);
                    break;
                case 'getEstatesByObject':
                    if (empty($attr['object']))
                        throw new Exception("Object type is not set");
                    $vars['estates'] = $this->api->getEstatesByObject($attr['object'], $attr);
                    break;
                case 'saleList':
                    break;
                case 'getDealUrlById':
                    $useJson = false;
                    $useTemplate = false;
                    if (empty($attr['deal']))
                        throw new Exception("Deal ID is not set");
                    if(!$model = $this->api->getDealById($attr['deal']))
                        throw new Exception("Deal ID is not found");
                    $content = $model->getViewUrl();
                    break;
                case 'dealPage':
                    if(empty($attr['deal']))
                        throw new Exception("Deal ID is not set");
                    if(!$model = $this->api->getDealById($attr['deal']))
                        throw new Exception("Deal ID is not found");
                    $content = $model->getViewUrl();
                    wp_redirect($content); exit;
                    break;
                case 'dealImage':
                    if(empty($attr['deal']))
                        throw new Exception("Deal ID is not set");
                    if (!$model = $this->api->getDealById($attr['deal']))
                        throw new Exception("Deal ID is not found");
                    $content = $model->getImageViewUrl();
                    wp_redirect($content); exit;
                    break;
                case 'getDealsByObject':
                    if (empty($attr['object']))
                        throw new Exception("Object type is not set");
                    $vars['deals'] = $this->api->getDealsByObject($attr['object'], $attr);
                    break;
                case 'getEstateById':
                    if (empty($attr['query']))
                        throw new Exception("Estate ID is not set");
                    $vars['estate'] = $this->api->getEstateById($attr['query']);
                    break;
                case 'getFileById':
                    if (empty($attr['query']))
                        throw new Exception("File ID is not set");
                    $vars['file'] = $this->api->getFileById($attr['query']);
                    break;
                case 'getCredentials':
                    if(empty($attr['token']))
                        throw new Exception("Token is not set");
                    $useJson = true;
                    $useTemplate = false;
                    $content = $this->retrieveCredentials($attr);
                    break;
                case 'query':
                    if (empty($attr['path']))
                        throw new Exception("Query path is not set");
                    if (empty($attr['var']))
                        throw new Exception("Variable name is not set");
                    $vars[$attr['var']] = $this->api->query($attr);
                    break;
                default:
                    if (isset($apiCalls[$attr['api']])) {
                        $vars[$apiCalls[$attr['api']]] = $this->api->$attr['api']($attr);
                    } else {
                        throw new Exception("'api' attribute value '{$attr['api']}' is not supported");
                    }
            }

            if ($useTemplate) {
                if (empty($attr['view']))
                    throw new Exception("Template file is not set");
                $content = $this->_renderView($attr['view'], $vars);
            }

            $output = array(
                'status' => 'success',
                'content' => $content,
            );
        } catch (Exception $e) {
            $output = array(
                'status' => 'error',
                'message' => $e->getMessage(),
            );
        }

        //this will work only if the deal update is called
        //this is async. after response it will continue updating the deal
        if($attr['clear'] && $attr['clear']=='deal') {
            exit;
        } else {
            if ($useJson) {
                header('Content-Type: application/json');
                echo json_encode($output);
            } else {
                if ($output['status'] == 'error') {
                    header('HTTP/1.1 500 Internal Server Error', true, 500);
                    echo $output['message'];
                } else {
                    header('Content-Type: application/json');
                    echo $output['content'];
                }
            }
            exit;
        }
    }

    public function retrieveCredentials($attr) {
        if($attr['token'] == $this->_getApi()->getInternalToken()) {
            $credentials['username'] = $this->_getApi()->getUser();
            $credentials['password'] = $this->_getApi()->getPassword();
            return $credentials;
        }
        else {
            $credentials['msg'] = 'wrong token';
        }
        return $credentials;
    }

    public function recache($file, $content)
    {
        @unlink($file);
        if(@$content['info']['url']){
            $this->api->query($content['info']['url']);
        }
    }

    public function _updateBidding($biddingId) {
        $api = $this->_getApi();
        $cache = MSPECS::getCache();
        $tags = array();

        $api->setForceCached(true);
        $api->setCallRequest(true);

        $tags[] = $biddingId;

        if($bidding = $api->getBiddingById($biddingId)) {
            if($bids = $bidding->getBids()) {
                foreach ($bids as $bid) {
                    $tags[] = $bid->getId();
                }
            }
        }

        $api->setForceCached(false);
        $api->setCallRequest(false);

        // echo '<pre>' . print_r($tags, true) . '</pre>';
        $cache->updateCacheByTags($tags, array($this, 'recache'), 'any');
        return $tags;
    }

    public function _updateCompanyTree() {
        $api = $this->_getApi();
        $cache = MSPECS::getCache();
        $tags = array();
        $api->setForceCached(true);
        $api->setCallRequest(true);

        $tags[] = 'organizations';
        if($company = $api->getCompany()) {
            $tags[] = $company->getId();
        }

        //add brokers tags
        //get broker access right id
        $accessRight = $api->getBrokers();
        $tags[] = 'contacts/getByAccessRight/' . $accessRight['id'];

        if($brokers = $api->getBrokers()) {
            foreach($brokers as $broker) {
                $tags[] = $broker->getId();
            }
        }

        $api->setForceCached(false);
        $api->setCallRequest(false);

        //echo '<pre>' . print_r($tags, true) . '</pre>';
        $cache->updateCacheByTags($tags, array($this, 'recache'), 'any');
        //update deals list
        //$cache->updateCacheByTags("deals", array($this, 'recache'));
        return $tags;
    }

    public function _updateDealStatic($dealId) {
        $api = $this->_getApi();
        $cache = MSPECS::getCache();
        $tags = array('deals list', $dealId);

        $tags[] = $dealId;
        $api->setForceCached(true);
        $api->setCallRequest(true);

        if ($deal = $api->getDealById($dealId)) {
            $tags[] = $deal->getCurrency();
            if ($files = $deal->getFiles()) {
                foreach ($files as $file) {
                    if ($file->getId()) {
                        $tags[] = $file->load()->getId();
                        $file->getUrl(true);
                    }
                }
            }
            if ($files = $deal->getPublicFiles()) {
                foreach ($files as $file) {
                    if ($file->getId()) {
                        $tags[] = $file->load()->getId();
                        $file->getUrl(true);
                    }
                }
            }
            if($links = $deal->getUrls()) {
                foreach($links as $link) {
                    if($link->getId()) {
                        $tags[] = $link->getId();
                    }
                }
            }
            if ($broker = $deal->getMainBroker()) {
                $tags[] = $broker->getId();
                if (($image = $broker->getProfileAvatar()) && ($imageId = $image->load()->getId())) {
                    $tags[] = $imageId;
                    $image->getImageUrl(true);
                    $image->getThumbnailUrl(true);
                }
            }
            
            if($bidding = $api->getBiddingById($deal->getBiddingId())) {
            $tags[] = $deal->getBiddingId();
            if($bids = $bidding->getBids()) {
                foreach ($bids as $bid) {
                        $tags[] = $bid->getId();
                    }
                }
            }

            if($viewings = $deal->getVisitingDates()) {
                foreach($viewings as $viewing) {
                    $tags[] = $viewing->getId();
                }
            }

            if($estate = $deal->getMainEstate()) {
                //recache easements and details
                if($easements = $estate->getEastemensAndDetails()) {
                    foreach($easements as $easement) {
                        $tags[] = $easement->getId();
                    }
                }
                
                //update tenants list
                if ($tenants = $estate->getTenants()) {
                    foreach ($tenants as $key => $value) {
                        $tags[] = $value->getId();
                    }
                }

                //recache farming
                if($estate->getObjectType() == 'OBJECT_TYPE_FARMING') {
                    if($areas = $estate->getAreas()) {
                        foreach($areas as $area) {
                            $tags[] = $area->getId();
                        }
                    }
                    //update farming specifics
                    if($farmingspecifics = $estate->getFarmingSpecifics()) {
                        foreach($farmingspecifics as $farmingSpecific) {
                            $tags[] = $farmingSpecific->getId();
                        }
                    }
                }
                //recache operating costs
                if($operatingCosts = $estate->getOperatingCosts()) {
                    foreach($operatingCosts as $operatingcost) {
                        $tags[] = $operatingcost->getId();
                    }
                }

                //recache everything for housing asos
                if($housingAsos = $estate->getHousingAssociation()) {
                    $tags[] = $housingAsos->getId();
                }

                if($patios = $estate->getPatios()) {
                    foreach($patios as $patio) {
                        $tags[] = $patio->getId();
                    }
                }

                if($parkings = $estate->getParkingSpaces()) {
                    foreach($parkings as $parking) {
                        $tags[] = $parking->getId();
                    }
                }
                //recache buildings
                if($estate->getObjectType() == 'OBJECT_TYPE_FARMING' || $estate->getObjectType() == 'OBJECT_TYPE_COMMERCIAL') {
                    if($buildings = $estate->getBuildings()) {
                        foreach($buildings as $building) {
                            $tags[]= $building->getId();
                        }
                    }
                }
                //recache mortgages
                if($mortgages = $estate->getMortgages()) {
                    foreach($mortgages as $mortgage) {
                        $tags[] = $mortgage->getId();
                    }
                }
                //recache rooms
                if($rooms = $estate->getRooms()) {
                    foreach($rooms as $room) {
                        $tags[] = $room->getId();
                    }
                }

                if($images = $estate->getPublicImages()) {
                    foreach($images as $image) {
                        if($image->getId()) {
                            $tags[] = $image->load()->getId();
                            $image->getImageUrl(true);
                            $image->getThumbnailUrl(true);
                        }

                    }
                }
            }

            if ($estate = $deal->getMainEstate()) {
                $categories = array(null, 'ENUMS_IMAGES_IN_ESTATES_CATEGORY_FLOOR_PLAN');
                foreach ($categories as $category) {
                    if ($images = $estate->getImages($category)) {
                        foreach ($images as $image) {
                            if ($imageId = $image->load()->getId()) {
                                $tags[] = $imageId;
                                $image->getImageUrl(true);
                                $image->getThumbnailUrl(true);
                            }
                        }
                    }
                }
            }
        }

        $api->setForceCached(false);
        $api->setCallRequest(false);

        //echo '<pre>' . print_r($tags, true) . '</pre>';
        $cache->updateCacheByTags($tags, array($this, 'recache'), 'any');
    }

    public function _isDealAccessable($dealId) {
        $api = $this->_getApi();
        $api->setNoCacheCall(true);
        $available = false;
        if($data = $api->getDealById($dealId)) {
            $available = true;
        } else {
            $available = false;
        }
        $api->setNoCacheCall(false);
        return $available;
    }

    public function _updateDeal($dealId)
    {
        if($this->_isDealAccessable($dealId)) {
            //detach client here
            ignore_user_abort(true);
            $content = array('code'=>'200', 'status'=>'success', 'message'=>"deal with id {$dealId} is updating now");
            $json = json_encode($content, JSON_PRETTY_PRINT);
            header("Connection: close");
            header("Content-Length: " . mb_strlen($json));
            header('Content-Type: application/json');
            echo json_encode($content, JSON_PRETTY_PRINT);
            flush(); // releasing the browser from waiting
            $this->_updateDealStatic($dealId);
        } else {
            //detach client here
            ignore_user_abort(true);
            $content = array('code'=>'401', 'status'=>'error', 'message'=>"no access to {$dealId}");
            $json = json_encode($content, JSON_PRETTY_PRINT);
            header("Connection: close");
            header("Content-Length: " . mb_strlen($json));
            header('Content-Type: application/json');
            echo json_encode($content, JSON_PRETTY_PRINT);
            flush();
        }
    }
}

$ajaxController = new Mspecs_Controller_Ajax();
