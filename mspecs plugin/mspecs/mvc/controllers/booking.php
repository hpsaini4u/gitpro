<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 28/10/14
 * Time: 14:01
 */

class Mspecs_Controller_Booking extends Mspecs_Controller {

    protected $_api = null;
    protected $connection = null;
    protected $_user = null;
    protected $_password = null;
    protected $_apiUrl = null;
    protected $_fields = array();
    protected $helper = null;
    protected $response = array();

    protected $_args = array(
        'firstname' => null,
        'lastname' => null,
        'email' => null,
        'phone' => null,
        'comment' => null,
        'viewing_id' => null,
        'deal_id' => null,
        'property_name' => null,
        'viewing_date' => null,
        'street_address' => null,
        'zip_code' => null,
        'city' => null,
        'broker_fname' => null,
        'broker_lname' => null,
        'broker_phone' => null,
        'broker_email' => null,
        'broker_company' => null,
        'booking_option' => null,
        'contact_id' => null,
        'company_id' => null
    );

    protected $error = array(
        1 => array(
            'status' => 'ERROR',
            'message' => 'wrong values in parameters',
            'code' => '300'
        ),
        2 => array(
            'status' => 'ERROR',
            'message' => 'invalid token',
            'code' => '400'
        )
    );

    public function __construct()
    {
        add_action('wp_ajax_booking', array($this, 'render'));
        add_action('wp_ajax_nopriv_booking', array($this, 'render'));

        $this->helper = new Mspecs_Helper();
    }

    protected function checkPostArguments() {
        foreach($this->_args as $key => $arg) {
            if(!isset($_POST[$key])) {
                $this->response['error'] = "param '" . $key . "' does not exist, please verify you are sending all params";
                return false;
            }
            $this->_args[$key] = $_POST[$key];
        }
        return true;
    }

    protected function checkDuplicateEmail($email, $viewingId) {
        $query = array(
            'path' => 'viewingAttendees',
            'query' => array(
                'q' => "viewingId='{$viewingId}' AND email='{$email}'"
            )
        );
        //to prevent dublicates we have to make api call without caching.
        $this->_getApi()->setNoCacheCall(true);
        $data = $this->_getApi()->queryOne($query);
        $this->_getApi()->setNoCacheCall(false);
        if($data == null) {
            return true;
        } else {
            return false;
        }
    }

    protected function bookViewing() {
        $this->_api = $this->_getApi();
        $this->_user = $this->_api->getUser();
        $this->_password = $this->_api->getPassword();
        $this->_apiUrl = $this->_api->getUrl();

        //retrieve params for token array
        $data = array(
            'deal_id' => $this->_args['deal_id'],
            'viewing_id' => $this->_args['viewing_id'],
            'contact_id' => $this->_args['contact_id'],
            'company_id' => $this->_args['company_id'],
            'email' => $this->_args['email'],
            'first_name' => $this->_args['firstname'],
            'last_name' => $this->_args['lastname'],
            'phone' => $this->_args['phone']
        );

        //generate token
        $data =  $this->helper->generateToken($data);
        //get base url of website
        $url = get_site_url() . "/wp-admin/admin-ajax.php?action=viewing&viewing=add&token=" . urlencode($data);
        $this->_args['link'] = $url;

        $mailer = new Mspecs_Controller_Mailer();
        return $mailer->sendTemplateEmail($this->bookingTemplate($this->_args), $this->_args['email'], $this->_args['broker_company'],
            "Bekräfta visningsbokning för " . $this->_args['street_address']);
    }

    protected function bookingTemplate($params) {
        $template = file_get_contents(plugin_dir_path(__FILE__) . "../../includes/templates/booking.html");
        //replace verification link
        $template = str_replace('<%verificationlink%>', $params['link'], $template);
        //replace customer name
        $template = str_replace('<%customername%>', $params['firstname'] . " " . $params['lastname'], $template);
        //replace property name
        $template = str_replace('<%propertyname%>', $params['property_name'], $template);
        //replace viewing date
        $template = str_replace('<%viewingdate%>', $params['viewing_date'], $template);
        //replace property address
        $address = $params['street_address'] . ", " . $params['zip_code'] . " " . $params['city'];
        $template = str_replace('<%propertyaddress%>', $address, $template);
        //comment
        /*$template = str_replace('<%comment%>', $params['comment'], $template);*/
        //replace company information
        $template = str_replace('<%brokername%>', $params['broker_fname'] . " " . $params['broker_lname'], $template);
        $template = str_replace('<%broker_email%>', $params['broker_email'], $template);
        $template = str_replace('<%broker_phone%>', $params['broker_phone'], $template);
        $template = str_replace('<%broker_company%>', $params['broker_company'], $template);
        //replace company address
        return $template;
    }

    public function render() {

        if(!$this->checkPostArguments()) {
            $this->error[1] = array_merge($this->error[1], $this->response);
            echo "<pre>" . json_encode($this->error[1], JSON_PRETTY_PRINT) . "</pre>";
        } else {
            //retrieve api params
            if($this->checkDuplicateEmail($this->_args['email'], $this->_args['viewing_id'])) {
                if($this->bookViewing()) {
                    echo "success";
                } else {
                    echo "error";
                }
            } else {
                echo "duplicate";
            }
        }

        exit;
    }
}

$bookingController = new Mspecs_Controller_Booking();