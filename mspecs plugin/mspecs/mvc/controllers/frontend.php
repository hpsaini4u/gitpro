<?php

class Mspecs_Controller_Frontend extends Mspecs_Controller
{

    protected $_data = null;

    public function __construct()
    {
        if (!is_admin() && MSPECS::getHelper()->getPage()) {
            add_action('wp', array($this, 'init'));
        }
    }

    public function init()
    {

        global $wp_query;

        if (is_page() && $wp_query->get_queried_object()->ID == MSPECS::getHelper()->getPage()->ID) {
            try {
                switch (@$_REQUEST['type']) {
                    case 'deal':
                        if (!isset($_REQUEST['deal_id']) || !($dealId = $_REQUEST['deal_id']))
                            throw new Exception('Deal ID is not set');
                        if (!($model = $this->_getApi()->getDealById($dealId)))
                            throw new Exception('Deal is not found');
                        if ($model->getIsPublished() || $model->getWebPageAccessibleOnlyByDirectUrl()) {
                          $this->_data = array(
                              'title' => $model->getDisplayName(),
                              'content' => $this->_renderView('deal', array('deal' => $model))
                          );
                        } else {
                          throw new Exception('Deal is not found');
                        }
                        break;
                    case 'estate':
                        if (!isset($_REQUEST['estate_id']) || !($estateId = $_REQUEST['estate_id']))
                            throw new Exception('Estate ID is not set');
                        if (!($model = $this->_getApi()->getEstateById($estateId)))
                            throw new Exception('Estate is not found');
                        $this->_data = array(
                            'title' => $model->getPropertyName(),
                            'content' => $this->_renderView('estate', array('estate' => $model))
                        );
                        break;
                    case 'viewing':
                        if(!isset($_REQUEST['viewing_id']) || !($viewingId = $_REQUEST['viewing_id'])) {
                            throw new Exception('Viewing ID is not set');
                        }
                        if (!($model = $this->_getApi()->getViewingById($viewingId)))
                            throw new Exception('Viewing is not found');

                        $this->_data = array(
                            'title' => "Visning Boka",
                            'content' => $this->_renderView('viewing_message', array('viewing' => $model))
                        );
                        break;
                    default:
                        throw new Exception('Unknown page');
                }

                add_filter('the_content', array($this, 'render'));
                add_filter('wp_title', array($this, 'setTitle'));
            } catch (Exception $e) {
                $wp_query->set_404();
                status_header(404);
            }
        }
    }

    public function setTitle($title)
    {
        if ($this->_data)
            return $this->_data['title'];
        return $title;
    }

    public function render($content)
    {
        if ($this->_data)
            return $this->_data['content'];
        return $content;
    }

}

$frontendController = new Mspecs_Controller_Frontend();
