<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 05/02/15
 * Time: 18:00
 */

class Mspecs_Controller_Mailer extends Mspecs_Controller {

    protected $config = null;
    protected $mail = null;

    protected $error = array(
        1 => array(
            'status' => 'ERROR',
            'message' => 'wrong command type',
            'code' => '340'
        ),
        2 => array(
            'status' => 'ERROR',
            'message' => 'no email option set',
            'code' => '370'
        )
    );

    public function __construct() {
        $config = MSPECS::getConfig()->get();
        $this->useGoogleCaptcha = $config['useGoogleCaptcha'];
        $this->googleCaptcha = $config['googleCaptcha'];
        $this->config = $config['mailer'];
        $this->mail = new PHPMailer(true);
        /** MAIL PARAMETERS **/
        $this->mail->IsSMTP(); // enable SMTP
        $this->mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
        $this->mail->MailerDebug = false;
        $this->mail->SMTPAuth = $this->config['SMTPAuth'];  // authentication enabled
        $this->mail->SMTPSecure = $this->config['SMTPSecure']; // secure transfer enabled REQUIRED for GMail in Office360 tls
        $this->mail->Host = $this->config['server'];
        $this->mail->Port = $this->config['port'];
        $this->mail->CharSet  = $this->config['charSet'];
        $this->mail->Username = $this->config['username'];
        $this->mail->Password = $this->config['password'];

        /** WORDPRESS AJAX LISTENERS **/
        add_action('wp_ajax_mailer', array($this, 'render'));
        add_action('wp_ajax_nopriv_mailer', array($this, 'render'));
    }

    public function render() {
        if(!$email = $_POST['mailer']) {
            echo "<pre>" . json_decode($this->error[2], JSON_PRETTY_PRINT) . "</pre>";
        } else {
            switch($email) {
                case 'interest':
                    //interest email
                    $this->sendInterestEmail();
                    break;
                case 'viewingBooking':
                    //update your viewing
                    break;
                default;
                    echo "<pre>" . json_encode($this->error[1], JSON_PRETTY_PRINT) . "</pre>";
            }
        }
        exit;
    }

    public function sendTemplateEmail($template, $email, $title, $subject) {
        $this->mail->SetFrom($this->config['username'], $title);
        $this->mail->Subject = $subject;

        $this->mail->msgHTML($template);
        /*$this->mail->Body = "Hej, jag är intresserad av ovanstående objekt! " . $params['link'];*/
        $this->mail->AddAddress($email);
        if($this->mail->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function sendViewingConfirmationEmail($params) {
        $this->mail->SetFrom($this->config['username'], "Visningsbokning bekräftad för" . $params['broker_company']);
        $this->mail->Subject = "Visningsbokning bekräftad för " . $params['street_address'];

    }

    protected function getInterestAttributes() {
        $params = array(
            'email' => null,
            'brokerEmail' => null,
            'firstName' => null,
            'lastName' => null,
            'phone' => null,
            'message' => null,
            'displayName' => null,
        );

        foreach($params as $key => $param) {
            if(!isset($_POST[$key])) {
                return false;
            }
            $params[$key] = $_POST[$key];
        }

        return $params;
    }

    public function sendInterestEmail() {

        //get all params and work if everything is correct
        if($params = $this->getInterestAttributes()) {

            // add captcha (and logic) if useCaptcha is set to true
            $useCaptcha = $this->useGoogleCaptcha;
            if($useCaptcha) {
                $recaptcha = 'g-recaptcha-response';
                $secret = $this->googleCaptcha['secretKey'];
                $response = $_POST[$recaptcha];
                $remoteip = $_SERVER['REMOTE_ADDR'];

                $url = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $response . "&remoteip=" . $remoteip);
                $result = json_decode($url, TRUE);
            }

            if($result['success'] == 1 || !$useCaptcha) {
                
                $this->mail->SetFrom($this->config['username'], $params['firstName'] . " " . $params['lastName']);
                $this->mail->Subject = "Intresseanmälan: " . $params['displayName'];
                $this->mail->Body = "Hej, jag är intresserad av " . $params['displayName']
                    . ".\n" . $params['message'] . "\n"
                    . $params['firstName'] . " " . $params['lastName']
                    . "\nTelefon: " . $params['phone']
                    . "\nE-post: " . $params['email'];

                $this->mail->AddReplyTo($params['email'], $params['firstName'] . " " . $params['lastName']);
                $this->mail->AddAddress($params['brokerEmail']);
                $this->mail->AddCC($params['email']);

                if($this->mail->send()) {
                    echo json_encode(array('status' => 'success', 'code' => 0), JSON_PRETTY_PRINT);
                } else {
                    //Not showing
                    echo json_encode(array('status' => 'error', 'code' => -1), JSON_PRETTY_PRINT);
                }

            } else {
                echo "Recpatcha invalid";
            }

            // $connect = curl_init();
            // curl_setopt($connect, CURLOPT_SSL_VERIFYPEER, false); // disable SSL verification
            // curl_setopt($connect, CURLOPT_RETURNTRANSFER, true);  // return response

        }
    }
}

$mailer = new Mspecs_Controller_Mailer();