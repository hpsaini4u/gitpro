<?php

class Mspecs_Controller_Shortcode extends Mspecs_Controller
{

    protected $webhome = null;

    public function __construct()
    {
        add_shortcode('mspecsapi', array($this, 'render'));
    }

    public function render($attr)
    {
        $api = MSPECS::getApi();
        $vars = array(
            'attributes' => $attr,
        );

        try {
            if (!is_array($attr) || sizeof($attr) == 0)
                throw new Exception("Shortcode attributes are not set");

            //set website options, such as menu, template
            //$attr = $this->setOption($attr, $api);

            if (!empty($attr['api'])) {

                $apiCalls = array(
                    'getBrokers' => 'brokers',
                    'getAssistants' => 'assistants',
                    'getCompany' => 'company',
                    'getDeals' => 'deals',
                    'getEstates' => 'estates',
                    'getUrls' => 'urls',
                    'getWebhome' => 'web',
                );

                //load template pages
                $template = 'WEBHOME_HEADER_TEMPLATE_LIST';

                switch ($attr['api']) {
                    case 'getDealById':
                        if (empty($attr['query']))
                            throw new Exception("Deal ID is not set");
                        $vars['deal'] = $api->getDealById($attr['query']);
                        break;
                    case 'getEstatesByObject':
                        if (empty($attr['object']))
                            throw new Exception("Object type is not set");
                        //load view template here
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars['estates'] = $api->getEstatesByObject($attr['object'], $attr);
                        break;
					case 'getDealsByObject':
						if (empty($attr['object']))
                            throw new Exception("Object type is not set");
                        //load view template here
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars['deals'] = $api->getDealsByObject($attr['object'], $attr);
                        break;
                    case 'getEstateById':
                        if (empty($attr['query']))
                            throw new Exception("Estate ID is not set");
                        //load view template here
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars['estate'] = $api->getEstateById($attr['query']);
                        break;
                    case 'getFileById':
                        if (empty($attr['query']))
                            throw new Exception("File ID is not set");
                        $vars['file'] = $api->getFileById($attr['query']);
                        break;
                    case 'query':
                        if (empty($attr['path']))
                            throw new Exception("Query path is not set");
                        if (empty($attr['var']))
                            throw new Exception("Variable name is not set");
                        $vars[$attr['var']] = $api->query($attr);
                        break;
                    case 'setHeader':
                        $vars['webhome'] = $api->getWebsiteOption('WEBHOME_HEADER_TEMPLATE_MENU');
                        break;
                    case 'setWebhomeSearch':
                        $vars['webhome'] = $api->getWebsiteOption('WEBHOME_HEADER_TEMPLATE_DEALS_SEARCH');
                        break;
                    case 'setWebhomeTemplate':
                        if(empty($attr['title']))
                            throw new Exception("title is not set");
                        $api->_pageTitle = $attr['title'];
                        $vars['webhome'] = $api->getWebsiteOption('WEBHOME_HEADER_TEMPLATE_CONTENT_TITLE');
                        break;
                    case 'getDeals':
                        //load view template and webhome search option here
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars[$apiCalls[$attr['api']]] = $api->$attr['api']($attr);
                        break;
                    case 'getDealsByMunicipality':
                        if(empty($attr['municipality']) && empty($attr['query']))
                            throw new Exception("municipality name is not set");
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars['deals'] = $api->getDealsByMunicipality($attr, $attr['municipality']);
                        break;
                    case 'getDealEstatesByObjectAndMunicipality':
                        if(empty($attr['municipality']) && empty($attr['query']))
                            throw new Exception("municipality name is not set");
                        if (empty($attr['object']))
                            throw new Exception("Object type is not set");
                        $attr['view'] = $this->setViewOption($api, $template, $attr['view']);
                        $vars['deals'] = $api->getDealEstatesByObjectAndMunicipality($attr, $attr['object'], $attr['municipality']);
                        break;
                    default:
                        if (isset($apiCalls[$attr['api']])) {
                            $vars[$apiCalls[$attr['api']]] = $api->$attr['api']($attr);
                        } else {
                            throw new Exception("'api' attribute value '{$attr['api']}' is not supported");
                        }
                }
            }

            if (empty($attr['view']))
                throw new Exception("Template file is not set");

            return $this->_renderView($attr['view'], $vars);
        } catch (Exception $e) {
            if (defined('WP_DEBUG') && WP_DEBUG) {
                return 'MSPECS API: ' . $e->getMessage();
            }
        }
        return '';
    }

    public function setViewOption($api, $option, $view) {
        //load options if empty
        if(!isset($this->webhome)) {
            $this->webhome = $api->getWebsiteOption($option);
        }

        //if option is loading the template
        if(isset($option) && isset($this->webhome)) {
            if($newview = $this->webhome->getWebhomeOption($option)) {
                return $newview;
            }
        }

        return $view;
    }

}

$shortcodeController = new Mspecs_Controller_Shortcode();