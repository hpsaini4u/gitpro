<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 05/02/15
 * Time: 15:41
 */

class Mspecs_Controller_Viewing extends Mspecs_Controller {

    protected $helper = null;

    protected $error = array(
        1 => array(
            'status' => 'ERROR',
            'message' => 'wrong command type',
            'code' => '340'
        ),
        2 => array(
            'status' => 'ERROR',
            'message' => 'no token set',
            'code' => '370'
        ),
        3 => array(
            'status' => 'ERROR',
            'message' => 'invalid token',
            'code' => '370'
        )
    );

    public function __construct() {
        //booking confirmations
        add_action('wp_ajax_viewing', array($this, 'render'));
        add_action('wp_ajax_nopriv_viewing', array($this, 'render'));

        $this->helper = new Mspecs_Helper();
    }

    public function render() {

        if(!$_GET['token']) {
            echo "<pre>" . json_decode($this->error[2], JSON_PRETTY_PRINT) . "</pre>";
        } else {
            switch($_GET['viewing']) {
                case 'add':
                    //add viewing;
                    $this->addViewing($_GET['token']);
                    break;
                case 'update':
                    //update your viewing
                    break;
                case 'cancel':
                    //cancel your viewing
                    break;
                default;
                    echo "<pre>" . json_decode($this->error[1], JSON_PRETTY_PRINT) . "</pre>";
            }
        }

        exit;
    }

    private function viewVerifiyTemplate($response) {

        //retrieve all necessary data
        $streetAddress = $response['deal']->getStreetAddress();
        $fullAddress = $response['deal']->getStreetAddress() . ", " . $response['deal']->getPostalCode() . " " . $response['deal']->getCity();
        $companyName = $response['company']->getName();
        $startDate = $response['viewing']->getStartDate();
        $brokerName = $response['contact']->getFirstName() . " " . $response['contact']->getLastName();
        $brokerPhone = $response['contact']->getPhoneNumber();
        $brokerEmail = $response['contact']->getEmail();

        $template = file_get_contents(plugin_dir_path(__FILE__) . "../../includes/templates/viewing_confirmation.html");

        $template = str_replace('<%streetAddress%>', $streetAddress, $template);
        $template = str_replace('<%fullAddress%>', $fullAddress, $template);
        $template = str_replace('<%companyName%>', $companyName, $template);
        $template = str_replace('<%startDate%>', $startDate, $template);
        $template = str_replace('<%brokerName%>', $brokerName, $template);
        $template = str_replace('<%brokerPhone%>', $brokerPhone, $template);
        $template = str_replace('<%brokerEmail%>', $brokerEmail, $template);

        return $template;
    }


    private function addViewing($token) {
        //convert url string to utf8
        $token = urldecode($token);
        //validate the token
        /***
         * response array
         * 0 - epoc
         * 1 - dealId
         * 2 - viewingId
         * 3 - broker contact id
         * 4 - company id
         * 5 - customer email
         * 6 - first name
         * 7 - last name
         * 8 - phone
         */
        if(!$response = $this->helper->validateToken($token)) {
            //$mailer->sendViewingConfirmationEmail()
            echo "<pre>" . json_encode($this->error[3], JSON_PRETTY_PRINT) . "</pre>";
        } else {

            if($this->postAttendee($response)) {
                //retrieve everything from
                $api = $this->_getApi();
                $api->setForceCached(false);
                $api->setCallRequest(false);
                $deal = $api->getDealById($response[1]);
                $viewing = $api->getViewingById($response[2]);
                $contact = $api->getContactById($response[3]);
                $company = $api->getCompanyById($response[4]);
                $params = array('deal' => $deal, 'viewing' => $viewing, 'contact' => $contact, 'company' => $company);

                $mailer = new Mspecs_Controller_Mailer();
                if($mailer->sendTemplateEmail($this->viewVerifiyTemplate($params), $response[5], $company->getName(), "Visningsbokning bekräftad för " . $deal->getStreetAddress())) {
                   /* header("Location: " . get_site_url());
                    die();*/
//                    echo $this->_renderView('confirm-message', array(
//                        'pages' => get_pages(array('hierarchical' => 0, 'parent' => 0))
//                    ));
//                    header_remove();
                    header('Location: ' . $this->getMessagePage($viewing->getId()));
                } else {
                    echo "error!";
                }

            } else {
                echo "error!";
            }

        }
    }

    public function getMessagePage($viewingId) {
        return MSPECS::getHelper()->getUrl(array(
            'type' => 'viewing',
            'status' => "success",
            'viewing_id' => $viewingId
        ));
    }

    private function postAttendee($response) {
        //index all values manually
        $data = array();
        $data['viewingId'] = $response[2];
        $data['firstName'] = $response[6];
        $data['lastName'] = $response[7];
        $data['email'] = $response[5];
        $data['phoneNumber'] = $response[8];

        $url = MSPECS::getApi()->getUrl() . "viewingAttendees";

        if($result = $this->_getPost()->post($data, $url)) {
            return true;
        } else {
            return false;
        }
    }

}

$viewingController = new Mspecs_Controller_Viewing();