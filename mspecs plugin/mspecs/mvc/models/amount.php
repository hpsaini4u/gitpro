<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 01/04/15
 * Time: 14:24
 */

class Mspecs_Model_Amount extends Mspecs_Model {

    public function economyTenant($amount, $objectType) {
        if($objectType=="OBJECT_TYPE_TENANT_OWNERSHIP") {
            return $this->inMonth($amount);
        } else {
            return $amount;
        }
    }

    public function inMonth($amount) {
        return round($amount / 12);
    }

} 