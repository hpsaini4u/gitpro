<?php

class Mspecs_Model_Area extends Mspecs_Model
{
    public function getTypeValue() {
        if($type = $this->getType()) {
            switch($type) {
                case "ENUM_AREA_TYPE_FIELD":
                    return "Åkermark";
                    break;
                case "ENUM_AREA_TYPE_OTHER":
                    return "Övrigmark";
                    break;
                case "ENUM_AREA_TYPE_CROPS":
                    return "Betesmark";
                    break;
                case "ENUM_AREA_TYPE_FISHING":
                    return "Fiskemark";
                    break;
                case "ENUM_AREA_TYPE_RENTAL_HOUSING":
                    return "Hyreshusmark bostad";
                    break;
                case "ENUM_AREA_TYPE_RENTAL_PREMISES":
                    return "Hyreshusmark lokal";
                    break;
                case "ENUM_AREA_TYPE_IMPEDIMENT":
                    return "Impediment";
                    break;
                case "ENUM_AREA_TYPE_INDUSTRIAL":
                    return "Industrimark";
                    break;
                case "ENUM_AREA_TYPE_INFIELDS":
                    return "Inägomark";
                    break;
                case "ENUM_AREA_TYPE_HUNTING_GROUND":
                    return "Jaktmark";
                    break;
                case "ENUM_AREA_TYPE_POWERPLANT":
                    return "Kraftverksmark";
                    break;
                case "ENUM_AREA_TYPE_WATERPOWER":
                    return "Mark på vattenfallsenhet";
                    break;
                case "ENUM_AREA_TYPE_FOREST":
                    return "Skogsmark";
                    break;
                case "ENUM_AREA_TYPE_IMPEDIMENT_FORREST":
                    return "Skogsimpediment";
                    break;
                case "ENUM_AREA_TYPE_HOUSING":
                    return "Småhusmark";
                    break;
                case "ENUM_AREA_TYPE_FARMING_HOUSE":
                    return "Småhusmark lantbruk";
                    break;
                case "ENUM_AREA_TYPE_EXTRACTION":
                    return "Täktmark";
                    break;
                case "ENUM_AREA_TYPE_WATER":
                    return "Vattenmark";
                    break;
                case "ENUM_AREA_TYPE_PLOT":
                    return "Mark på vattenfallsenhet";
                    break;
            }
        }
        return null;
    }

}