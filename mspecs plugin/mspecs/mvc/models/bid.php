<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 17/09/14
 * Time: 13:54
 */

class MSpecs_Model_bid extends Mspecs_Model {

    public function getBidTimeOnly($format = '%e %B, %G') {

        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE");
        if ($date = $this->getData('bidDate')) {
            $timespan = strtotime($date);
            return strftime($format, $timespan);
        }
        return null;
    }

    public function getAlias() {
        if($bidderId = $this->getBidderId()) {
            $query = array(
                'path' => 'bidders',
                'query' => array(
                    'q' => "id='{$bidderId}'"
                ),
            );

            $data = $this->_getApi()->queryOne($query);
            if(is_array($data)) {
                $buyerId = $data['buyerId'];
                $queryBuyer = array(
                    'path' => 'buyers',
                    'query' => array(
                        'q' => "id='{$buyerId}'"
                    ),
                );
                $dataBuyer = $this->_getApi()->queryOne($queryBuyer);
                if(is_array($dataBuyer)) {
                  $buyerGroupId = $dataBuyer['buyerGroupId'];
                  $queryBuyerGroup = array(
                    'path' => 'buyerGroups',
                    'query' => array(
                      'q' => "id='{$buyerGroupId}'"
                    ),
                  );
                  $dataBuyerGroups = $this->_getApi()->queryOne($queryBuyerGroup);
                   if(is_array($dataBuyerGroups)) {
                      return $dataBuyerGroups['bidderAlias'];
                   }
                }
            }
        }
    }

    public function getBidderStatusValue() {
        if ($bidderStatus = $this->getBidderStatus()) {
            $translated = '';
            switch($bidderStatus) {
                case "ENUMS_BIDDER_STATUS_ACTIVE";
                    $translated = 'Aktiv';
                    break;
                case "ENUMS_BIDDER_STATUS_DROPPED_OUT_WITH_INFORMATION":
                case "ENUMS_BIDDER_STATUS_DROPPED_OUT":
                    $translated = 'Hoppat av';
                    break;
                case "ENUMS_BIDDER_STATUS_STOPPED_WITH_INFORMATION":
                case "ENUMS_BIDDER_STATUS_STOPPED";
                    $translated = 'Stoppad';
                    break;
            }
            return $translated;
        }
    }
}
