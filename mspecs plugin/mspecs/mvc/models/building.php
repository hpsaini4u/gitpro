<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 09/06/14
 * Time: 14:52
 */

class Mspecs_Model_Building extends Mspecs_Model {

    protected $_buildingTypeEnums = null;
    protected $_parent = null;

    public function getEnergyStatusDateOnly($format = 'M j, Y H:i') {
        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('energyStatusDate')) {
            return date($format, strtotime($date));
        }
        return null;
    }

    public function getBuildingTypeEnum() {

      if (!$this->_buildingTypeEnums) {
        $query = array(
            'path' => 'buildingTypes',
            'query' => array(
                'q' => "id='{$this->getBuildingTypeId()}'"
            ),
        );
        $this->_buildingTypeEnums = $this->_getApi()->queryOne($query)['name'];
      }

      return $this->_buildingTypeEnums;
    }

    public function getBuildingTypeValue() {

        if($this->getBuildingTypeId()) {
            $data = $this->getBuildingTypeEnum();
            switch($data) {
                case "ENUM_BUILDING_TYPE_INDUSTRIAL_DEVELOPMENT":
                    return "Industribyggnad under uppförande";
                    break;
                case "ENUM_BUILDING_TYPE_STORAGE":
                    return "LagerAvkastning";
                    break;
                case "ENUM_BUILDING_TYPE_OFFICE":
                    return "Kontorsbyggnad";
                    break;
                case "ENUM_BUILDING_TYPE_PRODUCTION_STORAGE":
                    return "Produktionslokal";
                    break;
                case "ENUM_BUILDING_TYPE_INDUSTRIAL":
                    return "Industri";
                    break;
                case "ENUM_BUILDING_TYPE_RENTAL_PREMISES":
                    return "Hyreshusbyggnad lokal";
                    break;
                case "ENUM_BUILDING_TYPE_RENTAL_HOUSING":
                    return "Bostad hyreshusbyggnad bostad";
                    break;
                case "ENUM_BUILDING_TYPE_HOUSE_FARMING":
                    return "Småhusbyggnad lantbruk";
                    break;
                case "ENUM_BUILDING_TYPE_HOUSE":
                    return "Småhusbyggnad";
                    break;
                case "ENUM_BUILDING_TYPE_WATERPOWER":
                    return "Byggnad vattenfallsenhet";
                    break;
                case "ENUM_BUILDING_TYPE_VALUEPROPERTY":
                    return "Värderingsenhet";
                    break;
                case "ENUM_BUILDING_TYPE_POWERPLANT":
                    return "Byggnad övrigt kraftverk";
                    break;
                case "ENUM_BUILDING_TYPE_ECONOMI":
                    return "Ekonomibyggnad";
                    break;
            }
        }

        return null;
    }

    public function getEnergyClassValue() {
        if($this->getEnergyClass()) {
            switch($this->getEnergyClass()) {
                case "ENUMS_ENERGYCLASS_TYPE_A":
                    return "A";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_B":
                    return "B";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_C":
                    return "C";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_D":
                    return "D";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_E":
                    return "E";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_F":
                    return "F";
                    break;
                case "ENUMS_ENERGYCLASS_TYPE_G":
                    return "G";
                    break;
            }
        }
        return null;
    }

    public function getEnergyStatusValue() {
        if($this->getEnergyStatus()) {
            switch($this->getEnergyStatus()) {
                case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IS_NOT_NEEDED":
                    return "Energideklaration behövs ej";
                    break;
                case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IS_MADE":
                    return "Energideklaration är utförd";
                    break;
                case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IN_PROGRESS":
                    return "Energideklaration pågår";
                    break;
                case "ENUMS_EnergyStatus_TYPE_ENERGY_AUDITS_NOT_PERFORMED":
                    return "Energideklaration ej utförd";
                    break;
                case "ENUMS_EnergyStatus_TYPE_ENERGY_AUDITS_ARE_COMMISSIONED":
                    return "Energideklaration är beställd";
                    break;
            }
        }
        return null;
    }
    
    public function getParent() {
        if (!$this->getParentId()) {
            return null;
        }
        
        if (!$this->_parent) {
            $query = array(
            'path' => 'buildings',
            'query' => array(
                'q'=> "id='{$this->getParentId()}'"
            ));
            $parentData = $this->_getApi()->queryOne($query);
            if (is_array($parentData)) {
                $this->_parent = new Mspecs_Model_Building($parentData);
            }
        }
        
        return $this->_parent;
    }
    
    public function getParentName() {
        if ($parent = $this->getParent()) {
            return $parent->getName();
        }
        return null;
    }

    public function getOperatingCostAmounts() {
        $query = array(
            'path' => 'buildingOperatingCosts',
            'query' => array(
                'q' => "buildingId='{$this->getId()}'"
                ),
            );

        if($data = $this->_getApi()->query($query)) {
            if(is_array($data)) {
                //get amounts by costs
                $amountIds = array_map(function($cost) {
                    return $cost['amountId'];
                }, $data);

                if(!empty($amountIds)) {
                    $query = array(
                        'path' => 'amounts',
                        'query' => array(
                            'q' => "id IN ('" . join("','", $amountIds) . "')"),
                        );
                    if($amounts = $this->_getApi()->query($query)) {
                        $amountsList = array();
                        if(is_array($amounts) && !empty($amounts)) {
                            foreach ($amounts as $key => $value) {
                                $amountsList[] = new Mspecs_Model_Amount($value);
                            }
                        }
                        return new Mspecs_Collection($amountsList);
                    }
                }
            }
        }
        return null;
    }

    public function showOnlyComment() {
      if ($this->getBuildingTypeEnum() == 'ENUM_BUILDING_TYPE_ECONOMI') {
        return $this->getShowOnlyCommentInObjectDescription();
      } else if ($this->getBuildingTypeEnum() == 'ENUM_BUILDING_TYPE_HOUSE_FARMING') {
        return $this->getShowOnlyBuildingCommentInObjectDescription();
      }
      return null;
    }

    public function inMonth($amount) {
        return round($amount / 12);
    }

    public function inYear($amount) {
        return round($amount * 12);
    }
}
