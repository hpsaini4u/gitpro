<?php

class Mspecs_Model_Company extends Mspecs_Model
{
    public function getLogo()
    {
        if ($logoId = $this->getLogoFileId())
		{
			if($data=$this->_getApi()->queryOne(array('path' => 'files', 'query' => array('q' => "id='{$logoId}'"),))) {
				return $data['viewURI'];
			}
		}
		return null;
    }

    public function getLogoUrl() {
        if($webhome = $this->_getApi()->getWebsiteOption("WEBHOME_HEADER_LOGO_URL")) {
            if($body = $webhome->getBody()) {
                return $body;
            }
        }
        return null;
    }

}