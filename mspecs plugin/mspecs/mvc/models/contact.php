<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 21/08/14
 * Time: 10:27
 */

class Mspecs_Model_Contact extends Mspecs_Model {

    public function getProfileAvatar()
    {
        if ($imageId = $this->getProfilePictureId()) {
            return new Mspecs_Model_Image($imageId);
        }

        return null;
    }

    public function formatPhone($phoneNumber) {
        $phoneNumber = preg_replace('/[^0-9]/','',$phoneNumber);

        if(strlen($phoneNumber) > 10) {
            $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-9);
            $areaCode = substr($phoneNumber, -9, 2);
            $nextThree = substr($phoneNumber, -7, 3);
            $nextTwo = substr($phoneNumber, -4, 2);
            $lastTwo = substr($phoneNumber, -2, 2);

            $phoneNumber = '(+'.$countryCode.')'.$areaCode.'-'.$nextThree.' '.$nextTwo . ' ' . $lastTwo;
        }
        else if(strlen($phoneNumber) == 10) {
            $areaCode = substr($phoneNumber, 0, 3);
            $nextThree = substr($phoneNumber, 3, 3);
            $nextTwo = substr($phoneNumber, 6, 2);
            $lastTwo = substr($phoneNumber, 8, 2);

            $phoneNumber = $areaCode.'-'.$nextThree.' '.$nextTwo . ' ' . $lastTwo;

        }
        else if(strlen($phoneNumber) == 9) {
            $nextThree = substr($phoneNumber, 2, 1);
            $nextTwo = substr($phoneNumber, 3, 2);
            $otherTwo = substr($phoneNumber, 5, 2);
            $lastTwo = substr($phoneNumber, 7, 2);

            $phoneNumber = "0" . $nextThree.'-'.$nextTwo.' '.$otherTwo.' '.$lastTwo;
        }

        return $phoneNumber;
    }
}