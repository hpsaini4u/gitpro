<?php

class Mspecs_Model_Deal extends Mspecs_Model
{

    protected $_files = null;
    protected $_publicFiles = null;
    protected $_urls = null;
    protected $_mainEstate = null;
    protected $_currency = null;
    protected $_viewings = array();

    public function __construct($data = array())
    {
        if (is_array($data)) {
            $this->setData($data);
        }
    }

    public function getMainImage()
    {
        if ($imageId = $this->getMainImageFileId()) {
            return new Mspecs_Model_Image($imageId);
        }
        return null;
    }

    public function getDealImages()
    {
      $query = array(
        'path' => 'imagesInDeals',
        'query' => array(
          'q' => "dealId='{$this->getId()}'"
        )
      );
      if ($data = $this->_getApi()->query($query)) {
        $imagesInDeals = array();
        if (!empty($data)) {
          $imagesInDeals = array_map(function($obj) {
            return $obj['fileId'];
          }, $data);
        }

        if ($imagesData = ($this->_getApi()->query(array(
          'path' => 'files',
          'query' => array(
            'q' => "id IN ('" . join("','", $imagesInDeals) . "') AND isPublished=true"
          )
        )))) {
          $images = array();
          foreach ($imagesData as $key => $image) {
            array_push($images, new Mspecs_Model_Image($image));
          }

          return new Mspecs_Collection($images);
        }
      }
      return null;
    }

    public function getSellAsCompany() {
        if($this->getData('sellAsCompany')) {
            return "Överlåtes i bolagsform";
        }
    }

    public function getNewDevelopmentDeals() {
      $query = array(
        'path' => 'newDevelopmentDeals',
        'query' => array(
          'q' => "projectDealId='{$this->getId()}'"
        )
      );

      if ($data = $this->_getApi()->query($query)) {
        if (!empty($data)) {
          $newDevelopmentDeals = array_map(function($newDev) {
            return $newDev['dealId'];
          }, $data);

          if ($newDevDeals = ($this->_getApi()->query(array(
            'path' => 'deals',
            'query' => array(
              'q' => "id IN ('" . join("','", $newDevelopmentDeals) . "')"
            )
          )))) {
              $newDevDealsData = array();
              foreach ($newDevDeals as $key => $value) {
                array_push($newDevDealsData, new Mspecs_Model_Deal($value));
              }
              return new Mspecs_Collection($newDevDealsData);
          }
        }
      }
      return null;
    }

    public function getTeamRoles() {
        //fetch each teamRole through dealId
        $query = array(
            'path' => 'teamRoles',
            'query' => array(
                'q' => "dealId='{$this->getId()}'"
            )
        );
        //returns an array with all elements from prev query after applying contactId
        if ($teamRoles = $this->_getApi()->query($query)) {
            $contactIds = array();
            if (!empty($teamRoles)) {
                $contactIds = array_map(function($obj) {
                    return $obj['contactId'];
                }, $teamRoles);
                $query = "id IN ('" . join("','", $contactIds) . "')";
                if($contacts = ($this->_getApi()->query(array(
                    'path' => 'contacts',
                    'query' => array(
                        'q' => $query
                    )
                )))) {
                $brokers = array();
                foreach ($contacts as $key => $contact) {
                    # code...
                    foreach ($teamRoles as $key => $teamRole) {
                        # code...
                        //change to mainbroker instead of isPublic
                        if ($teamRole['contactId'] == $contact['id'] && $teamRole['isPublic'] == true) {
                            $contact['role'] = $teamRole['role'];
                            array_push($brokers, new Mspecs_Model_Contact($contact));
                        }
                    }
                }
                return new Mspecs_Collection($brokers);
                }
            }
        }

        return null;
    }

    public function getAssistant() {
        if($contacts = $this->getTeamRoles()) {
            foreach($contacts as $contact) {
                if($contact->getId() != $this->getMainBrokerContactId()) {
                    return $contact;
                }
            }
        }
        
        return null;
    }

    public function getUseGoogleCaptcha() 
    {
        $config = MSPECS::getConfig()->get();
        $useGoogleCaptcha = $config['useGoogleCaptcha'];
        return $useGoogleCaptcha;
    }

    public function getCaptchaPublic()
    {
        $config = MSPECS::getConfig()->get();
        $googleCaptcha = $config['googleCaptcha'];
        return $googleCaptcha['siteKey'];
    }

    public function getBidding()
    {
        if($this->getBiddingId()) {
            $query = array(
                'path' => 'biddings',
                'query' => array(
                    'q' => "id='{$this->getBiddingId()}'"
                ),
            );

            if ($data = $this->_getApi()->queryOne($query)) {
                return new Mspecs_Model_Bidding($data);
            }
        }
        return null;
    }

    //get list of the brokers for estate
    public function getBrokers()
    {
        if ($brokers = $this->_getApi()->getBrokers()) {
            return $brokers;
        }
        return null;
    }

    //get company logo
    public function getCompanyLogo()
    {
        return $this->_getApi()->getCompany()->getLogo();
    }

    //get main broker of the deal
    public function getMainBroker()
    {
        if($mainBrokerId = $this->getMainBrokerContactId()) {
            if($brokerContactId = $this->_getApi()->getContactById($mainBrokerId)) {
                return $brokerContactId;
            }
        }
        return null;
    }

    public function getStartingPriceTypeValue() {
        $value = null;
        if($startingPricetype = $this->getStartingPricetype()) {
            switch($startingPricetype) {
                case "ENUMS_ASKINGPRICETYPE_TYPE_OR_BY_APPOINTMENT":
                    $value = "Enligt ök";
                    break;
                case "ENUMS_ASKINGPRICETYPE_TYPE_HIGHEST_BIDDER":
                    $value = "Högstbjudande";
                    break;
                case "ENUMS_ASKINGPRICETYPE_TYPE_IS_ACCEPTED_PRICE":
                    $value = "Accepterat";
                    break;
                case "ENUMS_ASKINGPRICETYPE_TYPE_SOLD_BY_QUOTATION":
                    $value = "Säljs via anbudsförfarande";
                    break;
                default:
                    $value = $startingPricetype;
                    break;
            }
        }
        return $value;
    }

    public function getFiles()
    {
        $files = array();
        if (!$this->_files) {
            $query = array(
                'path' => 'filesInDeals',
                'query' => array(
                    'q' => "dealId='{$this->getId()}'"
                ),
            );
            //files in deals object
            if ($data = $this->_getApi()->query($query)) {
                if(is_array($data) && !empty($data)) {
                    $fileIds = array_map(function($obj) { return $obj['fileId'];}, $data);
                    if(!empty($fileIds)) {
                        $queryFiles = array(
                            'path' => 'files',
                            'query' => array(
                                'q' => "id IN ('" . join("','", $fileIds) . "')",
                                'sort' => 'displayOrder'
                            )
                        );

                        if($filesData = $this->_getApi()->query($queryFiles)) {
                            if(is_array($filesData) && !empty($filesData)) {
                                foreach($filesData as $file) {
                                    $files[] = new Mspecs_Model_File($file);
                                }
                            }
                        }
                    }
                }
            }
        }
        return new Mspecs_Collection($files);
    }

    public function getFileIds() {
        $query = array(
            'path' => 'filesInDeals',
            'query' => array(
                'q' => "dealId='{$this->getId()}'"
            ),
        );

        //files in deals object
        if ($data = $this->_getApi()->query($query)) {
            return array_map(function ($obj) { return $obj['fileId']; }, $data);
        }
        return null;
    }

    public function getPublicFiles() {

        $fileIds = $this->getFileIds();
        $files = array();
        if(is_array($fileIds)) {
            foreach($fileIds as $fileId) {
                if($file = $this->_getApi()->getFileById($fileId, true))
                    $files[]= $file;
            }
        }

        if(!empty($files))
            return $files;
        return null;
    }

    public function getTokenId() {
        if($tokenId = $this->getCommunicationTokenId()) {
            $query = array(
                'path' => 'communicationTokens',
                'query' => array(
                    'q' => "id='{$tokenId}'"));

            if($data = $this->_getApi()->queryOne($query)) {
                return $data['token'];
            }
        }

        return null;
    }

    public function getMainEstate()
    {
        if ($this->getMainEstateId()) {
            return $this->_getApi()->getEstateById($this->getMainEstateId());
        }
        return null;
    }

    public function getUrls()
    {
        if (!$this->_urls) {

            $query = array(
                'path' => 'dealUrls',
                'query' => array(
                    'q' => "dealId='{$this->getId()}' and isPublished=true",
                    'sort' => 'displayOrder'
                ),
            );

            //files in deals object
            if ($data = $this->_getApi()->getUrlsByDealId($query)) {
                if (sizeof($data) > 0) {
                    $this->_urls = $data;
                }
            }
        }
        return $this->_urls;
    }

    public function getViewUrl()
    {
        return MSPECS::getHelper()->getUrl(array(
                    'type' => 'deal',
                    'deal_id' => $this->getId()
                ));
    }
    
    public function getImageViewUrl() {
        return MSPECS::getHelper()->getUrl(array(
            'type' => 'deal',
            'deal_id' => $this->getId(),
            'property-image' => true
        ));
    }

    public function getObjectType() {
        if($this->getObjectTypeId()) {
            return $this->_getApi()->getObjectType($this->getObjectTypeId());
        }
    }

    public function getOrganization() {
        if($organization = $this->_getApi()->getCompany()) {
            return $organization;
        }
        return null;
    }

    public function getObjectStatusText() {
      $translatedObjectStatus = null;
      switch($this->getObjectStatus()) {
        case "ENUMS_OBJECTSTATUS_TYPE_IN_PROGRESS":
          $translatedObjectStatus = 'Pågående';
          break;
        case "ENUMS_OBJECTSTATUS_TYPE_REPOSSESSED":
        $translatedObjectStatus = 'Återtagen';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_ESTIMATE":
        $translatedObjectStatus = 'Värdering';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_DORMANT":
        $translatedObjectStatus = 'Vilande';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_RESERVED":
        $translatedObjectStatus = 'Reserverad';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_COMING":
        $translatedObjectStatus = 'Kommande';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_NO ASSIGNMENT":
        $translatedObjectStatus = 'Inget uppdrag';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_LOST_LISTING":
        $translatedObjectStatus = 'Förlorat intag';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_OTHER":
        $translatedObjectStatus = 'Annat';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_ARCHIVED":
        $translatedObjectStatus = 'Arkiverat';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_CLOSED":
        $translatedObjectStatus = 'Avslutat';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_REFERENCE":
        $translatedObjectStatus = 'Referensobjekt';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_FOR_SALE":
        $translatedObjectStatus = 'Till salu';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_INTAKE":
        $translatedObjectStatus = 'Under intag';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_MARKETING":
        $translatedObjectStatus = 'Annonsering';
        break;
        case "ENUMS_OBJECTSTATUS_TYPE_SOLD":
        $translatedObjectStatus = 'Såld';
        break;
      }

      return $translatedObjectStatus;
    }

    public function getQuotationDueDateOnly($format = '%e %B, %G') {

        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getQuotationDueDate()) {
            $timespan = strtotime($date);
            return strftime($format, $timespan);
        }
        return null;
    }

    public function getVisitingDates()
    {
        if(empty($this->_viewings)) {
            //get today date if proper format
            date_default_timezone_set('Europe/Stockholm');
            setlocale(LC_TIME, "sv_SE.UTF-8");
            $today = strtotime(date('M j, Y'));
            $query = array(
                'path' => 'viewings',
                'query' => array(
                    'q' => "dealId='{$this->getId()}' AND viaInternet=true",
                    'sort' => "startDate"
                )
            );
            if ($data = $this->_getApi()->query($query)) {
                foreach ($data as $view) {
                    //load if the date is today or more
                    $timespan = strtotime($view['startDate']);
                    if($timespan>=$today && !$view['contactBrokerForViewing']) {
                        $this->_viewings[] = new Mspecs_Model_Visit($view);
                    } else if($view['contactBrokerForViewing']) {
                        $this->_viewings[] = new Mspecs_Model_Visit($view);
                    } else if($view['displayOnlyViewingDescription']) {
                        $this->_viewings[] = new Mspecs_Model_Visit($view);
                    }
                }
            }
        }

        return $this->_viewings;
    }

    public function getFormattedDate($dateObject) {
      $time = strtotime($dateObject);
      date_default_timezone_set('Europe/Stockholm');
      setlocale(LC_TIME, "sv_SE");
      if (date('h', $time) === 0) {
        return strftime('%e %B, %G', $time);
      } else {
        return strftime('%e %B, %G', $time);
      }
    }

    public function getCrncy() {
        if(!$this->_currency) {
            $this->_currency = "kr";
            if($currencyId = $this->getCurrency()) {
                $query = array(
                    'path' => 'currencies',
                    'query' => array(
                        'q' => "id='{$currencyId}'"
                    ),
                );
                if($data = $this->_getApi()->queryOne($query)) {
                   switch($data['isoCode']) {
                       case 'SEK':
                           $this->_currency = 'kr';
                           break;
                       case 'EUR':
                           $this->_currency = "€";
                           break;
                       case 'USD':
                           $this->_currency = '$';
                           break;
                       default:
                           $this->_currency = "kr";
                           break;
                   }
                }
            }
        }
        return $this->_currency;
    }
}
