<?php

class Mspecs_Model_Easement_And_Details extends Mspecs_Model {

	public function getEasementTypeText() {
		$easementTypeText = '';
		if($easementType = $this->getEasementType()) {
			switch($easementType) {
				case 'ENUMS_EASEMENTTYPE_COMMUNITYFACILITIES':
				$easementTypeText = 'Gemensamhetsanläggning';
				break;

				case 'ENUMS_EASEMENTTYPE_ENROLLEDEASEMENT':
				$easementTypeText = 'Inskrivna övriga rättigheter, belastningar och övriga gravationer';
				break;

				case 'ENUMS_EASEMENTTYPE_PLANSANDREGULATIONS':
				$easementTypeText = 'Planer och bestämmelser';
				break;

				case 'ENUMS_EASEMENTTYPE_PREFERENTIALBURDEN':
				$easementTypeText = 'Rättigheter last';
				break;

				case 'ENUMS_EASEMENTTYPE_PREFERENTIALRIGHT':
				$easementTypeText = 'Rättigheter förmån';
				break;

				case 'ENUMS_EASEMENTTYPE_JOINT_PROPERTY':
				$easementTypeText = 'Samfällighet';
				break;

				case 'ENUMS_EASEMENTTYPE_ROADS':
				$easementTypeText = '';
				break;
			}
		}
		return $easementTypeText;
	}
}
