<?php

class Mspecs_Model_Estate extends Mspecs_Model
{
    protected $totalMortgage = 0;
    protected $countMortgage = 0;
    protected $_objectType = null;

    public function getFloorPlanImages()
    {
        return $this->getImages('ENUMS_IMAGES_IN_ESTATES_CATEGORY_FLOOR_PLAN');
    }

    public function getLocation() {
        // ugly as fuck but who cares ¯\_(ツ)_/¯
        if (!$this->getLocationDescriptionId()) {
            return null;
        }

        $query = array(
            'path' => 'locationDescriptions',
            'query' => array(
                'q' => "id='{$this->getLocationDescriptionId()}'"
            )
        );

        if ($data = $this->_getApi()->queryOne($query)) {
            if ($data['fileId']) {
                $fileQuery = array(
                    'path' => 'files',
                    'query' => array(
                        'q' => "id='{$data['fileId']}'"
                    )
                );
                if ($fileData = $this->_getApi()->queryOne($fileQuery)) {
                    return new Mspecs_Model_Image($fileData);
                }
            }
        } 

        return null;
    }

    public function getImages($category = null)
    {
        //not wanted default categories
        $categories = array(
//            "ENUMS_IMAGES_IN_ESTATES_CATEGORY_EXTERIOR" => true,
//            "ENUMS_IMAGES_IN_ESTATES_CATEGORY_INTERNAL" => true,
//            "ENUMS_IMAGES_IN_ESTATES_CATEGORY_MAP" => true
              "ENUMS_IMAGES_IN_ESTATES_CATEGORY_FLOOR_PLAN" => true
        );
        $q = "estateId='{$this->getId()}'";
        if ($category) {
            $q .= " and category='{$category}'";
        }

        $query = array(
            'path' => 'imagesInEstates',
            'query' => array(
                'q' => $q,
            ),
        );

        $files = array();

        if ($data = $this->_getApi()->query($query)) {
            if(is_array($data) && !empty($data)) {

                //remove with unwanted categories for default
                if(!$category) {
                    $imagesInEstate = array();
                    foreach($data as $imageInEstate) {
                        if(!isset($categories[$imageInEstate['category']])) {
                            $imagesInEstate[] = $imageInEstate;
                        }
                    }
                    $data = $imagesInEstate;
                }
                $fileIds = array_map(function($obj) { return $obj['fileId'];}, $data);
                if(!empty($fileIds)) {
                    $queryFiles = array(
                        'path' => 'files',
                        'query' => array(
                            'q' => "id IN ('" . join("','", $fileIds) . "')",
                            'sort' => 'displayOrder'
                        )
                    );
                    if($filesData = $this->_getApi()->query($queryFiles)) {
                        if(is_array($filesData)) {
                            foreach($filesData as $file) {
                                $files[] = new Mspecs_Model_Image($file);
                            }
                        }
                    }
                }
            }
        }
        return new Mspecs_Collection($files);
    }

    public function hasLift() {
        $liftEnum = $this->getHasLiftSetting();

        if ($liftEnum == 'ENUM_HAS_LIFT_SETTING_YES') {
            return 'Ja';
        } else if ($liftEnum == 'ENUM_HAS_LIFT_SETTING_NO') {
            return 'Nej';
        } else {
            return null;
        }
    }
    
    public function getTransferFeePaidByValue() {
        if (!$this->getTransferFeePaidBy()) {
            return null;
        }
        
        switch (($this->getTransferFeePaidBy())) {
            case "ENUMS_HOUSING_ASSOCIATION_TRANSFER_FEE_PAID_BY_BUYER":
                return 'Köpare';
            case "ENUMS_HOUSING_ASSOCIATION_TRANSFER_FEE_PAID_BY_SELLER":
                return 'Säljare';
            case "ENUMS_HOUSING_ASSOCIATION_TRANSFER_FEE_PAID_BY_NO_FEE":
                return 'Ingen avgift';
            default:
                return null;
        }
    }

    public function getPublicImages($category = null)
    {
        if ($allImages = $this->getImages($category)) {
            $images = array();
            foreach ($allImages as $image) {
                if ($image->load()->getIsPublished()) {
                    $images[] = $image;
                }
            }
            ksort($images);
            if (sizeof($images) > 0) {
                return new Mspecs_Collection($images);
            }
        }
        return null;
    }

    public function getMortgages() {
        $query = array(
            'path' => 'mortgages',
            'query' => array(
                'q' => "estateId='{$this->getId()}'",
                'sort' => 'displayOrder'
            )
        );
        $mortgages = array();
        if($data = $this->_getApi()->query($query)) {
            foreach($data as $mortgage) {
                //calculate total mortgage
                $this->totalMortgage += (float) $mortgage['amount'];
                $this->countMortgage++;
                $mortgages[] = new Mspecs_Model_Mortgage($mortgage);
            }
        }
        return new Mspecs_Collection($mortgages);
    }

    public function getTotalMortgage() {
        if($this->totalMortgage == 0) {
            $this->getMortgages();
        }
        return $this->totalMortgage;
    }

    public function getCountMortgage() {
        if($this->countMortgage==0) {
            $this->getMortgages();
        }
        return $this->countMortgage;
    }

	public function getFormattedDate($date) {
		$format = 'M j, Y';
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date) {
            return date($format, strtotime($date));
        }
        return null;
	}

	public function getRooms()
    {

        if($rooms = $this->_getApi()->getRoomsByEstate($this->getId())) {

            if(sizeof($rooms)>0) {
                return $rooms;
            }
        }
        return null;
    }

    public function getCountryName() {
        if($this->getCountryId()) {
            $query = array(
                'path' => 'countries',
                'query' => array(
                    'q' => "id='{$this->getCountryId()}'"));
            if($country = $this->_getApi()->queryOne($query)) {
                if(is_array($country)) {
                    return $country['name'];
                }
            }
        }
        return null;
    }
    
    public function getTenants() {
        $query = array('path' => 'estateTenants',
                'query' => array('q' => "estateId='{$this->getId()}'"));
              
        $tenantsList = array();
       if ($tenants = $this->_getApi()->query($query)) {
        foreach ($tenants as $key => $value) {
            array_push($tenantsList, new Mspecs_Model($value));
        }
       }  
       return new Mspecs_Collection($tenantsList);
    }
    
    public function getEstateTenantTotal() {
        // get estate tenants
        $query = array('path' => 'estateTenants',
                'query' => array('q' => "estateId='{$this->getId()}'"));
             
        if ($tenants = $this->getTenants()) {
            
            if (empty($tenants)) {
                return null;
            }
            
            // get estate tenant object statuses
            $objectStatuses = $this->_getApi()->query(array('path' => 'estateTenantsObjectTypesStatus'));
            $objectTypes = $this->_getApi()->query(array('path' => 'estateTenantsObjectTypes'));
            
            $objectStatusesIds = $this->_setObjectById($objectStatuses);
            $objectTypeIds = $this->_setObjectById($objectTypes);
            
            $sumTenant = array('area' => 0, 'income' => 0);
            
            $sum = array(
                'rented' => $sumTenant,
                'vacant' => $sumTenant,
                'reserved' => $sumTenant,
                'numberOfTenants' => 0,
                'numberOfHousing' => 0,
                'numberOfPremises' => 0,
                'totalArea' => 0,
                'totalIncome' => 0
            );
            // prepare rented tenants
            foreach ($tenants as $key => $tenantModel) {
                $sum['numberOfTenants']++;
                $area = 0;
                if ($objectTypeIds[$tenantModel->getEstateTenantsObjectTypeId()]["name"] == 'ESTATE_TENANTS_OBJECT_TYPES_PREMISES') {
                    $sum['numberOfPremises']++;
                    if ($tenantModel->getArea()) {
                        $area = $tenantModel->getArea();
                    } else {
                        $area = 0;
                    }
                } else {
                    $sum['numberOfHousing']++;
                    if ($tenantModel->getLivingArea()) {
                        $area = $tenantModel->getLivingArea();
                    } else {
                        $area = 0;
                    }
                }
                
                // MSPECS::getHelper()->printJson($objectStatusesIds);
                // echo $tenantModel->getStatus() . '<br/>';
                if ($tenantModel->getStatus())
                switch ($objectStatusesIds[$tenantModel->getStatus()]['name']) {
                    case 'ESTATE_TENANTS_OBJECTY_TYPES_STATUS_RENTED':
                        $sum['rented']['income'] += $tenantModel->getIncome();
                        $sum['rented']['area'] += $area;
                        $sum['totalArea'] += $area;
                        $sum['totalIncome'] += $tenantModel->getIncome();
                        break;
                    case 'ESTATE_TENANTS_OBJECTY_TYPES_STATUS_VACANT':
                        $sum['vacant']['income'] += $tenantModel->getIncome();
                        $sum['vacant']['area'] += $area;
                        $sum['totalArea'] += $area;
                        $sum['totalIncome'] += $tenantModel->getIncome();
                        break;
                        
                    case 'ESTATE_TENANTS_OBJECTY_TYPES_STATUS_RESERVED':
                        $sum['reserved']['income'] += $tenantModel->getIncome();
                        $sum['reserved']['area'] += $area;
                        $sum['totalArea'] += $area;
                        $sum['totalIncome'] += $tenantModel->getIncome();
                        break;
                }
            }
            
            return $sum;
        }
    }
    
    // converts object array into array with ids keys
    protected function _setObjectById($objectArray, $index = 'id') {
        $result = array();
        foreach ($objectArray as $key => $value) {
            $result[$value[$index]] = $value;
        }
        
        return $result;
    }

    public function getObjectTypeText() {
      $objectTypeText = '';
      switch($this->getObjectType()) {
        case 'OBJECT_TYPE_HOUSE':
          $objectTypeText = 'Småhus';
          break;
        case 'OBJECT_TYPE_TENANT_OWNERSHIP':
          $objectTypeText = 'Bostadsrätt';
          break;
        case 'OBJECT_TYPE_LAND':
          $objectTypeText = 'Tomt';
          break;
        case 'OBJECT_TYPE_COMMERCIAL':
          $objectTypeText = 'Kommersiell';
          break;
        case 'OBJECT_TYPE_HOUSING_DEVELOPMENT':
          $objectTypeText = 'Nyproduktion';
          break;
        case 'OBJECT_TYPE_RECREATIONAL_HOUSE':
          $objectTypeText = 'Fritidshus';
          break;
        case 'OBJECT_TYPE_CONDOMINIUM':
          $objectTypeText = 'Ägarlägenhet';
          break;
        case 'OBJECT_TYPE_PREMISE':
          $objectTypeText = 'Lokal';
          break;
      }

      return $objectTypeText;
    }

    public function getObjectTypeSubTypeText() {
        if ($this->getObjectSubTypeId()) {
          return $this->getSubObjectType();
        } else {
          return $this->getObjectTypeText();
        }
    }

    public function getImageByOrder($order)
    {
        if ($images = $this->getImages()) {
            foreach ($images as $image) {
                if ($image->load()->getDisplayOrder() == $order) {
                    return $image;
                }
            }
        }
        return null;
    }

    public function inMonth($amount) {
        return round($amount / 12);
    }

    public function economyTenant($amount) {
        if($this->getObjectType()=="OBJECT_TYPE_TENANT_OWNERSHIP") {
            return $this->inMonth($amount);
        } else {
            return $amount;
        }
    }

    public function period() {
        if($this->getObjectType()=="OBJECT_TYPE_TENANT_OWNERSHIP") {
            return "/mån";
        } else {
            return "/år";
        }
    }

    public function getMainImage()
    {
        if ($imageId = $this->getMainImageFileId()) {
            return new Mspecs_Model_Image($imageId);
        }
        return null;
    }

    public function getBuildings() {
        if($this->getObjectTypeId()) {
            if($this->getObjectType()=='OBJECT_TYPE_COMMERCIAL' || $this->getObjectType()=='OBJECT_TYPE_FARMING') {
                if($buildings = $this->_getApi()->getBuildings($this->getId(), $this->getObjectType())) {
                    if(sizeof($buildings)>0) {
                        return $buildings;
                    }
                }
            }
        }
        return null;
    }
    
    public function getValuationUnits() {
        if($this->getObjectType()=='OBJECT_TYPE_COMMERCIAL') {
            if($buildings = $this->_getApi()->getValuationUnits($this->getId())) {
                if(sizeof($buildings)>0) {
                    return $buildings;
                }
            }
        }
        return null;
    }

    public function getFarmingSpecifics() {
        if($this->getObjectType()=='OBJECT_TYPE_FARMING') {
            if($farmingSpecifics = $this->_getApi()->getFarmingSpecifics($this->getId())) {
                if(sizeof($farmingSpecifics)>0) {
                    return $farmingSpecifics;
                }
            }
        }
        return null;
    }

    public function getAreas() {
        if($objectId = $this->getObjectTypeId()) {
            if($this->getObjectType()==='OBJECT_TYPE_FARMING') {
                if($areas = $this->_getApi()->getAreas($this->getId())) {
                    if(sizeof($areas)>0) {
                        return $areas;
                    }
                }
            }
        }
        return null;
    }

    public function getEnergyStatusDateOnly($format = 'M j, Y') {
        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('energyStatusDate')) {
            return date($format, strtotime($date));
        }
        return null;
    }

    public function getEnergyStatusText() {
        $energyStatus = null;
        switch($this->getEnergyStatus()) {
            case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IS_NOT_NEEDED":
                $energyStatus = "Energideklaration behövs ej";
                break;
            case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IS_MADE":
                $energyStatus = "Energideklaration är utförd";
                break;
            case "ENUMS_EnergyStatus_TYPE_ENERGY_USE_IN_PROGRESS":
                $energyStatus = "Energideklaration pågår";
                break;
            case "ENUMS_EnergyStatus_TYPE_ENERGY_AUDITS_NOT_PERFORMED":
                $energyStatus = "Energideklaration ej utförd";
                break;
            case "ENUMS_EnergyStatus_TYPE_ENERGY_AUDITS_ARE_COMMISSIONED":
                $energyStatus = "Energideklaration är beställd";
                break;
        }
        return $energyStatus;
    }

    public function getEnergyClassOnly() {

        switch($this->getEnergyClass()) {
            case "ENUMS_ENERGYCLASS_TYPE_A":
                return "A";
                break;
        case "ENUMS_ENERGYCLASS_TYPE_B":
            return "B";
                break;
        case "ENUMS_ENERGYCLASS_TYPE_C":
                return "C";
                break;
        case "ENUMS_ENERGYCLASS_TYPE_D":
                return "D";
                break;
        case "ENUMS_ENERGYCLASS_TYPE_E":
                return "E";
                break;
        case "ENUMS_ENERGYCLASS_TYPE_F":
                return "F";
                break;
            case "ENUMS_ENERGYCLASS_TYPE_G":
                return "G";
            break;
        }
        return null;
    }

    public function getObjectType() {
        if(!$this->_objectType) {
            if($this->getObjectTypeId()) {
                $this->_objectType = $this->_getApi()->getObjectType($this->getObjectTypeId());
            }
        }
        return $this->_objectType;
    }

    // public function getObjectTypeName() {
    //     if($this->getObjectTypeId()) {
    //         echo $this->getObjectTypeId();
    //         return $this->_getApi()->getObjectType($this->getObjectTypeId());
    //     }

    //     return null;
    // }

    public function getTenureObjectType() {
      if ($this->getIsTenureHouse()) {
        if ($text = $this->getObjectSubTypeText()) {
          return $text;
        }
      }

      return 'Lägenhet';
    }

    public function getSubObjectType() {
        if($data = $estateSubObject = $this->_getApi()->queryOne(array(
            'path' => 'objectSubTypes',
            'query' => array(
                'q' => "id='{$this->getObjectSubTypeId()}'"
            ),
        ))) {
            // will fill rest of the types later
            $objectSubTypeText = '';
            switch($data['name']) {
                case 'OBJECTSUBTYPES_COMMERCIAL_HOTEL':
                    $objectSubTypeText = 'Hotel';
                    break;
                case 'OBJECTSUBTYPES_FARMING_CROPS':
                    $objectSubTypeText = 'Spannmål';
                    break;
                case 'OBJECTSUBTYPES_FARMING_FOREST':
                    $objectSubTypeText = 'Skog';
                    break;
                case 'OBJECTSUBTYPES_FARMING_HORSEFARM':
                    $objectSubTypeText = 'Hästgård';
                    break;
                case 'OBJECTSUBTYPES_FARMING_LEISURE':
                    $objectSubTypeText = 'Fritid';
                    break;
                case 'OBJECTSUBTYPES_FARMING_MEAT':
                    $objectSubTypeText = 'Kött';
                    break;
                case 'OBJECTSUBTYPES_FARMING_MILK':
                    $objectSubTypeText = 'Mjölk';
                    break;
                case 'OBJECTSUBTYPES_FARMING_PIGS':
                    $objectSubTypeText = 'Svin';
                    break;
                case 'OBJECTSUBTYPES_HOUSE_ATRIUM':
                  $objectSubTypeText = 'Atriumhus';
                  break;
                case 'OBJECTSUBTYPES_HOUSE_DETACHEDHOUSE':
                  $objectSubTypeText = 'Kedjehus';
                  break;
                case 'OBJECTSUBTYPES_HOUSE_SEMI-DETACHED':
                  $objectSubTypeText = 'Parhus';
                  break;
                case 'OBJECTSUBTYPES_HOUSE_TOWNHOUSE':
                  $objectSubTypeText = 'Radhus';
                  break;
               case 'OBJECTSUBTYPES_HOUSE_VILLA':
                  $objectSubTypeText = 'Villa';
                  break;

                case 'OBJECTSUBTYPES_COMMERCIAL_INDUSTRIAL':
                $objectSubTypeText = 'Industri';
                break;

                case 'OBJECTSUBTYPES_COMMERCIAL_LIVINGSPACE':
                $objectSubTypeText = 'Bostad';
                break;

                case 'OBJECTSUBTYPES_COMMERCIAL_OFFICE':
                $objectSubTypeText = 'Kontor';
                break;

                case 'OBJECTSUBTYPES_COMMERCIAL_RESTAURANT':
                $objectSubTypeText = 'Restaurang';
                break;

                case 'OBJECTSUBTYPES_COMMERCIAL_RETAIL':
                $objectSubTypeText = 'Butik';
                break;

                case 'OBJECTSUBTYPES_COMMERCIAL_WAREHOUSE':
                $objectSubTypeText = 'Lager';
                break;

                case 'OBJECTSUBTYPES_COMMERCIALMISES':
                $objectSubTypeText = 'Lokal';
                break;

                case 'OBJECTSUBTYPES_CONDOMINIUM':
                $objectSubTypeText = 'Ägarlägenhet';
                break;

                case 'OBJECTSUBTYPES_CONDOMINIUM_APARTMENT':
                $objectSubTypeText = 'Lägenhet';
                break;

                case 'OBJECTSUBTYPES_CONDOMINIUM_STORAGE':
                $objectSubTypeText = 'Lokal';
                break;

                case 'OBJECTSUBTYPES_CONDOMINIUM_SEMI-DETACHED':
                $objectSubTypeText = 'Parhus';
                break;

                case 'OBJECTSUBTYPES_CONDOMINIUM_TOWNHOUSE':
                $objectSubTypeText = 'Radhus';
                break;

                case 'OBJECTSUBTYPES_HOUSING_COOPERATIVE_APARTMENT':
                $objectSubTypeText = 'Lägenhet';
                break;

                case 'OBJECTSUBTYPES_HOUSING_COOPERATIVE_STORAGE':
                $objectSubTypeText = 'Lokal';
                break;

                case 'OBJECTSUBTYPES_HOUSING_COOPERATIVE_TOWNHOUSE':
                $objectSubTypeText = 'Radhus';
                break;

                case 'OBJECTSUBTYPES_HOUSING_COOPERATIVE_VILLA':
                $objectSubTypeText = 'Villa';
                break;

                case 'OBJECTSUBTYPES_RECREATIONAL_HOUSE_ATRIUM':
                $objectSubTypeText = 'Atriumfritidshus';
                break;

                case 'OBJECTSUBTYPES_RECREATIONAL_HOUSE_DETACHEDHOUSE':
                $objectSubTypeText = '';
                break;

                case 'OBJECTSUBTYPES_RECREATIONAL_HOUSE_TOWNHOUSE':
                $objectSubTypeText = 'Radfritidshus';
                break;

                case 'OBJECTSUBTYPES_RECREATIONAL_HOUSE_VILLA':
                $objectSubTypeText = 'Fritidshus';
                break;

                case 'OBJECTSUBTYPES_RECREATIONAL_HOUSE_SEMI-DETACHED':
                $objectSubTypeText = 'Parfritidshus';
                break;

                case 'OBJECTSUBTYPES_HOUSING_COOPERATIVE_CONDOMINIUM':
                $objectSubTypeText = 'Ägarlägenhet';
                break;

                case 'OBJECTSUBTYPES_PREMISE_PREMISE_FOR_RENT':
                $objectSubTypeText = 'Lokaluthyrning';
                break;

                case 'OBJECTSUBTYPES_PREMISE_PREMISE_FOR_SALE':
                $objectSubTypeText = 'Hyreslokal säljes';
                break;

                case 'OBJECTSUBTYPES_PREMISE_BUSINESS_FOR_SALE':
                $objectSubTypeText = 'Företag säljes';
                break;

                case 'OBJECTSUBTYPES_PREMISE_TENANT_PREMISE_FOR_SALE':
                $objectSubTypeText = 'BR-lokal säljes';
                break;

                case 'OBJECTSUBTYPES_OTHER':
                if($this->getObjectSubTypeText()) {
                    $objectSubTypeText = $this->getObjectSubTypeText();
                }
                else{
                    $objectSubTypeText = "Övrigt";
                }
                break;
            }
        }
        return $objectSubTypeText;
    }

    public function getBusinessTradeTypeValue() {
        if($value = $this->getBusinessTradeType()) {
            $tradeType = '';
            switch($value) {
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_SHOPS";
                    $tradeType = 'Butiker';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_CONSTRUCTION":
                    $tradeType = 'Byggverksamhet';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_HOTEL/TOURIST":
                    $$tradeType = 'Hotell/Turistföretag';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_RESTAURANTS/CAFES":
                    $tradeType = 'Restauranger/Caféer';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_MANUFACTURING":
                    $tradeType = 'Kontaktperson';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_SERVICE_COMPANY":
                    $tradeType = 'Tjänsteföretag';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_OTHER":
                    $tradeType = 'Övrigt';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_COMMERCE/E-COMMERCE":
                    $tradeType = 'Handel/e-handel';
                    break;
                case "EENUM_ESTATE_BUSINESS_TRADE_TYPE_CARE_AND_HEALTH":
                    $tradeType = 'Vård & Hälsa';
                    break;
                case "ENUM_ESTATE_BUSINESS_TRADE_TYPE_CARRIER":
                    $tradeType = 'Transportföretag';
                    break;
            }
            return $tradeType;
        }
    }

    public function getAssessValue() {
        if($this->getAssessValueTotal()) {
            return $this->getAssessValueTotal();
        } else {
            return intval($this->getAssessValueLand()) + intval($this->getAssessValueBuilding());
        }
    }

    public function getMainPurposes() {
        if($farmings = $this->_getApi()->getFarmingMainPurposes($this->getId())) {
            return $farmings;
        }
        return null;
    }

    public function getOperatingCosts() {
        $amounts = array();
        $query = array(
            'path' => 'estateOperatingCosts',
            'query' => array(
                'q' => "estateId='{$this->getId()}'"
            ),
        );
        if($operatingCosts = $this->_getApi()->query($query)) {
            if(!empty($operatingCosts)) {
                $amountIds = array_map(function($obj) { return $obj['amountId']; }, $operatingCosts);
                if(!empty($amountIds)) {
                    $queryAmounts = array(
                        'path' => 'amounts',
                        'query' => array(
                            'q' => "id IN ('" . join("','", $amountIds) . "')"
                        )
                    );
                    if($amountsData = $this->_getApi()->query($queryAmounts)) {
                        if(is_array($amountsData)) {
                            foreach($amountsData as $amount) {
                                $amounts[] = new Mspecs_Model_Amount($amount);
                            }
                        }
                    }
                }
            }
        }
        return new Mspecs_Collection($amounts);
    }

    public function getPatios() {
        if($patios = $this->_getApi()->getPatios($this->getId())) {
            return $patios;
        }
        return null;
    }

    public function getParkingSpaces() {
        if($parkingSpaces = $this->_getApi()->getParkingSpaces($this->getId())) {
            if(count($parkingSpaces)>0) {
                return $parkingSpaces;
            }
        }
        return null;
    }

    public function getViewUrl()
    {
        return MSPECS::getHelper()->getUrl(array(
                    'type' => 'estate',
                    'estate_id' => $this->getId(),
                ));
    }

    public function getHousingAssociation() {
        if($housingAsossiacionId = $this->getHousingAssociationId()) {
            return $this->_getApi()->getHousingAsociationById($housingAsossiacionId);
        }
        return null;
    }

    public function getPremiseExtras() {
        $amounts = array();
        $query = array(
            'path' => 'estatePremiseEconomyAmounts',
            'query' => array(
                'q' => "estateId='{$this->getId()}'"
            ),
        );
        if($data = $this->_getApi()->query($query)) {
            //retrieved array of relations table
            //get all amount ids on it
            if(is_array($data)) {
                $amountIds = array_map(function ($obj) { return $obj['amountId']; }, $data);
                if(!empty($amountIds)) {
                    $queryAmounts = array(
                        'path' => 'amounts',
                        'query' => array(
                            'q' => "id IN ('" . join("','", $amountIds) . "')"
                        )
                    );
                    if($amountsData = $this->_getApi()->query($queryAmounts)) {
                        if(is_array($amountsData)) {
                            foreach($amountsData as $amount) {
                                $amounts[] = new Mspecs_Model_Amount($amount);
                            }
                        }
                    }
                }
            }
        }
        return new Mspecs_Collection($amounts);
    }

    public function getPremiseIncludedCost() {
        $rentSum = 0;
        if($extras = $this->getPremiseExtras()) {
            foreach($extras as $extra) {
                $rentSum += $extra->getAmount();
            }
        }
        $rentSum += $this->getMonthlyRent() * 12;
        return $rentSum;
    }

    public function getEastemensAndDetails() {
      if ($this->getId()) {
        return $this->_getApi()->getEasementsAndDetailsByEstateId($this->getId());
      }
      return null;
    }

    public function showEconomySection() {
      $showEconomy = false;
      $fieldsToCheck = array('insuranceFee', 'repairFund', 'isWaterIncluded', 'isHeatIncluded', 'cableTVIncluded',
      'isElectricityIncluded', 'waterAndDrainFee', 'heatingCost', 'chimneySweepingCost', 'communityFee', 'maintenanceCost',
      'roadFee', 'sanitationFee', 'electricityConsumption', 'electricityFee', 'securityCost', 'pedgingFee', 'monthlyRent',
      'monthlyRentSummary', 'feeChanges', 'premiseIndex');

      foreach ($fieldsToCheck as $key => $value) {
        if ($this->getData($value) != null && $this->getData($value) != '') {
          $showEconomy = true;
          break;
        }
      }
      return $showEconomy;
    }

    public function showBuildingSection() {
        $showBuilding = false;
        $fieldsToCheck = array('landComment', 'buildYear', 'renovateYear', 'renovateDescription', 'ventilationType', 'ventilationInspectionDate',
        'windowsType', 'chimneyType', 'drainType', 'waterType', 'roofingType', 'radonType', 'foundationType', 'heatingType', 'heatingPlantCondition',
        'heatingPlantAge', 'heatingPlantBrand', 'heatingComment', 'electricalStemsCondition', 'mainFuse', 'electricSafety',
        'electricalComment', 'hasBurglarAlarm', 'internetSummary', 'hasCableTv', 'patioComment', 'otherBuildings', 'faultsAndDefectsComment',
        'buildingComment', 'otherAreasComment', 'upcomingRenovationsDescription'
      );

      foreach ($fieldsToCheck as $key => $value) {
        if ($this->getData($value) != null && $this->getData($value) != '') {
          $showBuilding = true;
          break;
        }
      }
      return $showBuilding;
    }
    
    public function getAllIncludedInRent() {
        $includedInRentTextArray = array();
        
        if ($this->getIsHeatIncluded()) {
            array_push($includedInRentTextArray, 'Uppvärmning ');
        }
        
        if ($this->getIsWaterIncluded()) {
            array_push($includedInRentTextArray, 'Vatten');
        }
        
        if ($this->getIsElectricityIncluded()) {
            array_push($includedInRentTextArray, 'El');
        }
        
        if ($this->getIsCableTVIncluded()) {
            array_push($includedInRentTextArray, 'Kabel-TV');
        }
        
        if ($this->getIsInternetIncluded()) {
            array_push($includedInRentTextArray, 'Internet');
        }
        
        if ($this->getIsCoolingIncluded()) {
            array_push($includedInRentTextArray, 'Kyla');
        }
        
        if (sizeof($includedInRentTextArray)>0) {
            return join(', ', $includedInRentTextArray);
        }
        
        return null;
    }
}
