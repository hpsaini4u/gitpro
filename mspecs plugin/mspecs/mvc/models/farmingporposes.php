<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 10/06/14
 * Time: 09:37
 */

class Mspecs_Model_FarmingPurposes extends Mspecs_Model {

    public function getNameValue() {
        if($name = $this->getName()) {
            switch($name) {
                case 'ENUM_FARMING_PURPOSE_LEISURE':
                    return 'Fritid';
                    break;
                case 'ENUM_FARMING_PURPOSE_HORSEFARM':
                    return 'Hästgård';
                    break;
                case 'ENUM_FARMING_PURPOSE_MEAT':
                    return 'Kött';
                    break;
                case 'ENUM_FARMING_PURPOSE_MILK':
                    return 'Mjölk';
                    break;
                case 'ENUM_FARMING_PURPOSE_FORESTRY':
                    return 'Skog';
                    break;
                case 'ENUM_FARMING_PURPOSE_CROPS':
                    return 'Spannmål';
                    break;
                case 'ENUM_FARMING_PURPOSE_PIGS':
                    return 'Svin';
                    break;
                case 'ENUM_FARMING_PURPOSE_OTHER':
                    return 'Övrigt';
                    break;
            }
        }
        return null;
    }
}