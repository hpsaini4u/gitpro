<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 24/07/14
 * Time: 14:14
 */

class Mspecs_Model_FarmingSpecifics extends Mspecs_Model {
    public function getTypeValue() {
        if($type = $this->getType()) {
            switch($type) {
                case "ENUM_FARMING_SPECIFICS_TYPE_ACQUISITION":
                    return "Förvärvstillstånd";
                break;
                case "ENUM_FARMING_SPECIFICS_TYPE_INSPECTIONEXAMINATIONDUTY":
                    return "Besiktning och undersökningsplikt";
                break;

                case "ENUM_FARMING_SPECIFICS_TYPE_FORESTDETAILS":
                    return "Skogsuppgifter";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_ARABLELAND":
                    return "Åkermark";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_PASTURE":
                    return "Betesmark";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_LEASESUPPORTCONCESSIONS":
                    return "Arrende/stöd/nyttjanderätter";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_ENVIRONMENTALSUPPORTCOMMITMENTS":
                    return "Miljöstöd och åtaganden";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_NATURALASSETS":
                    return "Naturvärden";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_ANCIENTCULTURALRELICS":
                    return "Forn- och kulturlämningar";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_HUNTING":
                    return "Jakt";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_FISHING":
                    return "Fiske";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_REALPROPERTY":
                    return "Fastighetsbildning";
                    break;
                case "ENUM_FARMING_SPECIFICS_TYPE_OTHER":
                    return "Övrigt";
                    break;
            }
        }
        return null;
    }
}