<?php

class Mspecs_Model_File extends Mspecs_Model
{

    protected $_loaded = false;

    public function __construct($data = null)
    {
        if (is_array($data)) {
            parent::__construct($data);
        } elseif ($data) {
            parent::__construct(array('fileId' => $data));
        }
    }

    public function getUrl($reload = false)
    {
        //do not load any files besides images
        if(!$this->load()->getIsImage()) {
            if ($url = $this->load()->getOriginalURI()) {
                //return $url;
                return MSPECS::getCache()->getFileUrl($url, $reload);
            }
        } else {
            if ($url = $this->load()->getViewURI()) {
                return MSPECS::getCache()->getFileUrl($url, $reload);
            }
        }

        return '';
    }

    public function getCategory()
    {
        if ($this->getFileCategoryId()) {
            $categories = array(
                'FILES_CATEGORY_3D_OBJECT'	=> '3D objekt',
                'FILES_CATEGORY_AERIAL_FILM' => 'Flygfilm',
                'FILES_CATEGORY_AERIAL_PHOTO' 	=> 'Flygfoto',
                'FILES_CATEGORY_AREA_FILM'		=> 'Områdesfilm',
                'FILES_CATEGORY_CALCULUS'		=> 'Kalkyl',
                'FILES_CATEGORY_CAMPAIGN'		=> 'Kampanj',
                'FILES_CATEGORY_COMPILATION'		=> 'Skog - sammanställning areal och virkesförråd',
                'FILES_CATEGORY_CUTTING_CLASS'		=> 'Skog - sammanställning huggningsklass',
                'FILES_CATEGORY_ECONOMICS'		=> 'Ekonomi',
                'FILES_CATEGORY_EQUIPMENT'		=> 'Utrustning',
                'FILES_CATEGORY_FIELD'		=> 'Åker',
                'FILES_CATEGORY_FILM'		=> 'Film',
                'FILES_CATEGORY_FLOORPLAN'		=> 'Planlösning',
                'FILES_CATEGORY_FOREST'		=> 'Skog',
                'FILES_CATEGORY_FOREST_AGE_GROUP'		=> 'Skog - sammanställning åldersklass',
                'FILES_CATEGORY_FOREST_COMPILATION_TARGET_CLASS'	=> 'Skog - sammanställning målklass',
                'FILES_CATEGORY_FOREST_PLAN'		=> 'Skogsbruksplan',
                'FILES_CATEGORY_FOREST_TITLE_LIST'		=> 'Skog - avdelningslista',
                'FILES_CATEGORY_FORM_BIDDING'		=> 'Blankett - bud',
                'FILES_CATEGORY_FORM_INTEREST'		=> 'Blankett - intresseanmälan',
                'FILES_CATEGORY_FORM_QUOTATION'		=> 'Blankett - anbud',
                'FILES_CATEGORY_GUIDE'		=> 'Guide',
                'FILES_CATEGORY_INFO'		=> 'Infoblad',
                'FILES_CATEGORY_INSPECTION'		=> 'Besiktning',
                'FILES_CATEGORY_LAW'		=> 'Lag',
                'FILES_CATEGORY_LIVINGFACTS'		=> 'Bostadsfakta',
                'FILES_CATEGORY_MAP_FOREST'		=> 'Karta - Skog',
                'FILES_CATEGORY_MAP_OTHER'		=> 'Karta - Övrig',
                'FILES_CATEGORY_MAP_OVERVIEW'		=> 'Karta - Översikt',
                'FILES_CATEGORY_MAP_PROPERTY'		=> 'Karta - Fastighet',
                'FILES_CATEGORY_OTHER'		=> 'Annat',
                'FILES_CATEGORY_PANORAMA'		=> 'Panorering',
                'FILES_CATEGORY_PHOTO_STYLING'		=> 'Fotostyling',
                'FILES_CATEGORY_PROPERTY_EXCERPT'		=> 'Fastighetsutdrag',
                'FILES_CATEGORY_PROPERTY_MAP'		=> 'Fastighetskarta',
                'FILES_CATEGORY_PROSPECT'		=> 'Prospekt',
                'FILES_CATEGORY_RADON'		=> 'Radon',
                'FILES_CATEGORY_SAMPLE_FURNISHING'		=> 'Provmöblering',
                'FILES_CATEGORY_SECTION_FORESTRY'		=> 'Skogsbruk',
                'FILES_CATEGORY_SITUATIONPLAN'		=> 'Situationsplan',
                'FILES_CATEGORY_SLIDE_SHOW'		=> 'Bildvisning',
                'FILES_CATEGORY_TECHNICAL'		=> 'Teknik',
            );
            $category = $this->_getApi()->getFileCategory($this->getId());
            if(isset($categories[$category])){
                return $categories[$category];
            }
            return 'Ingen kategori';
        }
        return "";
    }

    public function load()
    {
        if (!$this->_loaded) {
            if ($file = $this->_getApi()->getFileById($this->getFileId(), false)) {
                $this->addData($file->getData());
                $this->_loaded = true;
            }
        }
        return $this;
    }

    public function __toString()
    {
        return $this->getUrl();
    }

}