<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 24/07/14
 * Time: 08:51
 */

class Mspecs_Model_Housingassociation extends Mspecs_Model {

    protected $_publishedFiles = array();

    public function getImages($category = null) {
        $q = "housingAssociationId='{$this->getId()}'";
        $query = array(
            'path' => 'imagesInHousingAssociations',
            'query' => array(
                'q' => $q,
            ),
        );
        $images = array();
        if ($data = $this->_getApi()->query($query)) {
            if(is_array($data)) {
                foreach ($data as $fileData) {
                    $images[] = new Mspecs_Model_Image($fileData);
                }
            }
            return new Mspecs_Collection($images);
        }
        return null;
    }

    public function getFiles() {
        return $this->getPublishedFiles();
    }

    public function getPublishedFiles() {
        $query = array(
            'path' => 'filesInHousingAssociations',
            'query' => array(
                'q' => "housingAssociationId='{$this->getId()}'"
            ),
        );
        //files in deals object
        $files = array();
        if ($data = $this->_getApi()->query($query)) {

            //get file ids array
            $fileIds = array_map(function($obj) { return $obj['fileId'];}, $data);

            if(!empty($fileIds)) {
                $queryFiles = array(
                            'path' => 'files',
                            'query' => array(
                                'q' => "id IN ('" . join("','", $fileIds) . "') AND isPublished=true",
                                'sort' => 'displayOrder'
                            )
                        );
                if($filesData = $this->_getApi()->query($queryFiles)) {
                    if(is_array($filesData) && !empty($filesData)) {
                        foreach($filesData as $file) {
                            $files[] = new Mspecs_Model_File($file);
                        }
                    }
                }
            }
        }
        return new Mspecs_Collection($files);
    }
}