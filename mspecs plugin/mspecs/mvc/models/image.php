<?php

MSPECS::includeFile('mvc/models/file.php');

class Mspecs_Model_Image extends Mspecs_Model_File
{

    public function getImageUrl($reload = false)
    {
        if ($url = $this->load()->getViewURI()) {
            return MSPECS::getCache()->getFileUrl($url, $reload);
        }
        return '';
    }

	public function getDesc()
	{
		if($desc = $this->load()->getDescription()) {
			return $desc;
		}
		return '';
	}

	public function getOrder()
	{
		if($order = $this->load()->getDisplayOrder()) {
			return $order;
		}
		return null;
	}

    public function getThumbnailUrl($reload = false)
    {
        if ($url = $this->load()->getThumbnailURI()) {
            return MSPECS::getCache()->getFileUrl($url, $reload);
        }
        return $this->getImageUrl($reload);
    }

    public function __toString()
    {
        return $this->getImageUrl();
    }

}