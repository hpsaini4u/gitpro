<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 24/02/15
 * Time: 13:22
 */

class Mspecs_Model_Mortgage extends Mspecs_Model
{
    protected $_currency = null;

    public function __construct($data = array())
    {
        if (is_array($data)) {
            $this->setData($data);
        }
        $this->loadCurrencySymbol();
    }

    public function loadCurrencySymbol() {
        //load default swedish currency
        $this->_currency = "kr";
        if($currencyId = $this->getCurrency()) {
            $query = array(
                'path' => 'currencies',
                'query' => array(
                    'q' => "id='{$currencyId}'"
                ),
            );
            if($data = $this->_getApi()->queryOne($query)) {
                //echo "<pre>" . print_r($data, true) . "</pre>";
                switch($data['isoCode']) {
                    case 'SEK':
                        $this->_currency = 'kr';
                        break;
                    case 'EUR':
                        $this->_currency = "€";
                        break;
                    case 'USD':
                        $this->_currency = '$';
                        break;
                    default:
                        $this->_currency = "kr";
                        break;
                }
            }
        }
    }

    public function getRegistrDateOnly($format = '%e %B, %G') {
        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE");
        if ($date = $this->getData('registrationDate')) {
            $timespan = strtotime($date);
            return strftime($format, $timespan);
        }
        return null;
    }

    public function getCrncy() {
        return $this->_currency;
    }
}