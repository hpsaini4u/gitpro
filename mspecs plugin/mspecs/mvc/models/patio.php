<?php
/**
 * Created by PhpStorm.
 * User: rasulniyazimbetov
 * Date: 06/11/14
 * Time: 13:08
 */

class Mspecs_Model_Patio extends Mspecs_Model {

    public function getDirectionValue() {
        $value = "";
        if($direction = $this->getDirection()) {
            switch($direction) {
                case "ENUMS_DIRECTION_EAST":
                    $value = "Öst";
                    break;
                case "ENUMS_DIRECTION_WEST":
                    $value = "Väst";
                    break;
                case "ENUMS_DIRECTION_SOUTH":
                    $value = "Syd";
                    break;
                case "ENUMS_DIRECTION_SOUTHWEST":
                    $value = "Sydväst";
                    break;
                case "ENUMS_DIRECTION_SOUTHEAST":
                    $value = "Sydost";
                    break;
                case "ENUMS_DIRECTION_NORTH":
                    $value = "Norr";
                    break;
                case "ENUMS_DIRECTION_NORTHWEST":
                    $value = "Nordväst";
                    break;
                case "ENUMS_DIRECTION_NORTHEAST":
                    $value = "Nordost";
                    break;
                default:
                    break;
            }
        }
        return $value;
    }

    public function getPatioTypeValue() {
        $value = "";
        if($patio = $this->getPatioType()) {
            switch($patio) {
                case "ENUMS_PATIOTYPE_PATIO":
                    $value = "Uteplats";
                    break;
                case "ENUMS_PATIOTYPE_TERRACE":
                    $value = "Terrass";
                    break;
                case "ENUMS_PATIOTYPE_BALCONY":
                    $value = "Balkong";
                    break;
                case "ENUMS_PATIOTYPE_ALTAN":
                    $value = "Altan";
                    break;
                default:
                    break;
            }
        }

        return $value;
    }

}