<?php

class Mspecs_Model_Url extends Mspecs_Model
{
    public function getTrimmUrl() {

        $trimmUrl = $this->getUrl();
        if (!preg_match("~^(?:f|ht)tps?://~i", $this->getUrl())) {
            $trimmUrl = "http://" . $this->getUrl();
        }

        return $trimmUrl;
    }

}