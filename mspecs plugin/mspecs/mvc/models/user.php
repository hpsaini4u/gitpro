<?php

class Mspecs_Model_User extends Mspecs_Model
{

    protected $_contactInfo = null;
    protected $_loaded = false;

    public function load()
    {
        if (!$this->_loaded && ($id = $this->getUserAccountId())
                && ($data = $this->_getApi()->queryOne(array('path' => 'userAccounts', 'query' => array('q' => "id='{$id}'"))))) {
            $this->addData($data);
            $this->_loaded = true;
        }

        return $this;
    }

    public function getContactInfo($field = null)
    {
        if (!$this->_contactInfo && ($contactId = $this->load()->getContactId())
                && ($data = $this->_getApi()->queryOne(array('path' => 'contacts', 'query' => array('q' => "id='{$contactId}'"))))) {
            $this->_contactInfo = new Mspecs_Model($data);
        }
        if ($field) {
            if ($this->_contactInfo) {
                return $this->_contactInfo->getData($field);
            } else {
                return null;
            }
        }
        return $this->_contactInfo;
    }
	
	public function getProfilePicture()
	{
		if($imageId = $this->getProfilePictureId()) {
			return new Mspecs_Model_Image($imageId);
		}
		return null;
	}

    public function getProfileAvatar()
    {
        if ($imageId = $this->getContactInfo('profilePictureId')) {
            return new Mspecs_Model_Image($imageId);
        }
        return null;
    }

}