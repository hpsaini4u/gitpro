<?php

class Mspecs_Model_Visit extends Mspecs_Model
{

    public function getStartDate($format = 'M j, Y H:i')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('startDate')) {
            return date($format, strtotime($date));
        }
        return null;
    }

    public function getEndDate($format = 'M j, Y H:i')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('endDate')) {
            return date($format, strtotime($date));
        }
        return null;
    }

	public function getStartDateOnly($format = '%e %B, %G')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE");
        if ($date = $this->getData('startDate')) {
			$timespan = strtotime($date);
            return strftime($format, $timespan);
        }
        return null;
    }
    public function isAtleastToday($format = '%e %B, %G')
    {
        date_default_timezone_set('Europe/Stockholm');
        setlocale(LC_TIME, "sv_SE");
        if ($date = $this->getData('startDate')) {
            $timespan = strtotime($date);
            $today = strtotime(date('M j, Y'));
            if($timespan<$today) {
                return false;
            } else {
                return true;
            }
        }
        return fasle;
    }

    public function getEndDateOnly($format = '%e %B, %G')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE");
        if ($date = $this->getData('endDate')) {
            $timespan = strtotime($date);
            return strftime($format, $timespan);
        }
        return null;
    }

	public function getStartTime($format = 'H:i')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('startDate')) {
			$h = date('H', strtotime($date));
			$m = date('i', strtotime($date));
			if($h==0 && $m==0) {
				return null;
			}
            return date($format, strtotime($date));
        }
        return null;
    }

	public function getEndTime($format = 'H:i')
    {
		date_default_timezone_set('Europe/Stockholm');
		setlocale(LC_TIME, "sv_SE.UTF-8");
        if ($date = $this->getData('endDate')) {
            $h = date('H', strtotime($date));
			$m = date('i', strtotime($date));
			if($h==0 && $m==0) {
				return null;
			}
            return date($format, strtotime($date));
        }
        return null;
    }

}