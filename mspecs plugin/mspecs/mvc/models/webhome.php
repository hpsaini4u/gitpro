<?php

class Mspecs_Model_Webhome extends Mspecs_Model
{
    //keep all options in one package
    protected $options = array(
        //set header menu options
        "WEBHOME_HEADER_TEMPLATE_MENU" => array(
            'ENUM_TEMPLATE_ALWAYS_SHOW' => 0,
            'ENUM_TEMPLATE_ALWAYS_HIDE' => 1,
            'ENUM_TEMPLATE_ALWAYS_SHOW_LOGO' => 2,
            'ENUM_TEMPLATE_SHOW_LOGO_ON_DEAL' => 3
        ),
        //set template view loading options
        "WEBHOME_HEADER_TEMPLATE_LIST" => array(
            'ENUM_TEMPLATE_GRID' => 'deals',
            'ENUM_TEMPLATE_LIST' => 'deals-iframe'
        ),
        //set template for showing deal search
        "WEBHOME_HEADER_TEMPLATE_DEALS_SEARCH" => array(
            'ENUM_TEMPLATE_ALWAYS_HIDE' => 0,
            'ENUM_TEMPLATE_ALWAYS_SHOW' => 1
        ),
        "WEBHOME_HEADER_TEMPLATE_CONTENT_TITLE" => array(
            'ENUM_TEMPLATE_HIDE' => 0,
            'ENUM_TEMPLATE_SHOW' => 1
        )
    );

    protected $_option = null;
    protected $_search = null;

    public function getWebhomeOption($option) {
        if($header = $this->getHeader()) {
            if($header == $option) {
                if($value = $this->options[$option]) {
                    if($this->getBody()) {
                        if($value[$this->getBody()])
                            return $value[$this->getBody()];
                    }
                }
            }
        }
        return null;
    }

    public function getSearchOption() {
        if(!$this->_search) {
            $webhomeheader = "WEBHOME_HEADER_TEMPLATE_DEALS_SEARCH";
            if($option = $this->getWebhomeOption($webhomeheader)) {
                if(isset($option) && !empty($option)) {
                    $this->_search = $option;
                }
            }
        }

        return $this->_search;
    }

    public function getMenuOption() {
        if(!$this->_option) {
            $webhomeheader = "WEBHOME_HEADER_TEMPLATE_MENU";
            if($option = $this->getWebhomeOption($webhomeheader)) {
                if(isset($option) && !empty($option)) {
                    $this->_option = $option;
                }
            }
        }

        return $this->_option;
    }

    public function getTitleOption() {
        if(!$this->_option) {
            $webhomeheader = "WEBHOME_HEADER_TEMPLATE_CONTENT_TITLE";
            if($option = $this->getWebhomeOption($webhomeheader)) {
                if(isset($option) && !empty($option)) {
                    $this->_option = $option;
                }
            }
        }
        return $this->_option;
    }

    public function getTitle() {
        return $this->_getApi()->_pageTitle;
    }

	public function getPageImage()
    {
		$imageHeader = "";
		if($header = $this->getHeader()) {
			switch($header) {
				case "WEBHOME_HEADER_PAGE_ABOUT":
					$imageHeader = "WEBHOME_HEADER_IMAGE_PAGE_ABOUT";
					break;
				case "WEBHOME_HEADER_PAGE_SELL":
					$imageHeader = "WEBHOME_HEADER_IMAGE_PAGE_SELL";
					break;
				case "WEBHOME_HEADER_PAGE_BUY":
					$imageHeader = "WEBHOME_HEADER_IMAGE_PAGE_BUY";
					break;
                case "WEBHOME_HEADER_PAGE_STARTPAGE":
                    $imageHeader = "WEBHOME_HEADER_IMAGE_PAGE_STARTPAGE";
                    break;
                case "WEBHOME_HEADER_PAGE_FOREIGN":
                    $imageHeader = "WEBHOME_HEADER_IMAGE_PAGE_FOREIGN";
                    break;
				default:
					break;
			}

			$q = "header='{$imageHeader}'";

			$query = array(
				'path' => 'webhome',
				'query' => array(
					'q' => $q,
				),
			);

			if ($data = $this->_getApi()->queryOne($query)) {
				$imageWebhome = new Mspecs_Model_Webhome($data);
				return $imageWebhome->getBody();
			}			
		}

		return null;
	}
}