test upload script
By default you can use requests to api using shorttags in content:

[mspecsapi view="{template_name}"]

"view" attribute is required as it's used to point to template file, located 'plugin_directory/mvc/views/{template_name}.phtml'

[mspecsapi api="getDeals"]
view {template name}
api [getDealById, getDeals, getEstateById, getEstates, getFileById, query]
query - query string or id for getDealById/getEstateById
limit
offset
sort
var - userd only when api="query" and uses for creating php variable in template



Examples:

Get list of deals: [mspecsapi api="getDeals" limit="5" view="deals"]

Get single deal: [mspecsapi api="getDealById" query="MDA0NXwwMDAwMDAwMDAyMnw1OA.." view="deal"]

Get list of sold estates:
url: https://demo.mspecs.se/deals?q=objectStatus=%27ENUMS_OBJECTSTATUS_TYPE_CLOSED%27%20and%20isPublished=true&sort=updatedDate"
shortcode: [mspecsapi api="getDeals" query="q=objectStatus='ENUMS_OBJECTSTATUS_TYPE_CLOSED' and isPublished=true" sort="updatedDate" view="deals"]







By the way, I didn't see any attributes in models, will it keep attributes of json objects in views?

Yep, all JSON attributes will remain intact in model, you can get access to any JSON property using getter.
For example, if you want to show estate's `propertyName` use `$estate->getPropertyName()`, i.e. use `->get[NameOfProperty]`
with property's name first letter uppercase. In some cases when you can't get access to property using getter, use direct
query to get property `$model->getData('propertyName')` to get direct access to field.
To get all available fields as array, use `$model->getData()`

Besides this you can set model to process any property with additional operations, for example, DEAL model has method
`getMainEstate` which returns ESTATE model, and ESTATE model has methods `getMainImage` which returns array data with
link to image/thumbnail url and other information.

