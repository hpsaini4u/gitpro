<?php /* Template Name: details */ 
get_header();


$id= $_GET['id'];
global $wpdb;
$new_result = $wpdb->get_results("SELECT * FROM deals where id = '$id' ");
$primaryId = $new_result[0]->id;
//echo "<pre>";
//echo $id;
//echo "SELECT * FROM filesInDeals as s JOIN files as c where s.fileId=c.id and s.dealId= '".$id."' and c.viewURI !=''";

$photo_result = $wpdb->get_results("SELECT * FROM filesInDeals as s JOIN files as c where s.fileId=c.id and s.dealId= '".$id."' and c.viewURI !=''");
//print_r($photo_result);die;
$address = $new_result[0]->streetAddress;
$startingPrice = $new_result[0]->startingPrice;
?>

<section class="banner-section_detail">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<!--ol class="carousel-indicators">
		  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		  <li data-target="#myCarousel" data-slide-to="1"></li>
		  <li data-target="#myCarousel" data-slide-to="2"></li>
		  <li data-target="#myCarousel" data-slide-to="3"></li>
		</ol-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
	 <div class='item active'><img src='<?php bloginfo("template_directory"); ?>/assets/img/askengren1.jpg' width='1260' height='80' /></div>
	 <div class='item'><img src='<?php bloginfo("template_directory"); ?>/assets/img/askengren2.jpg' /></div>
	</div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
<?php

$broker_details = $wpdb->get_results("SELECT contactId FROM sellers where dealId = '".$id."'");
$primId = $broker_details[0]->contactId;

$abc = $wpdb->get_results("SELECT * FROM contacts where id = '".$primId."'");
//echo "<pre>";
//print_r($abc);
//echo "<pre>";
//print_r($broker_details);
?>
        <section class="listing-detail-section">
            <div class="heading-info-box">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-9 col-md-8">
                            <h1><span><i class="fa fa-home"></i></span><?php echo $new_result[0]->displayName." ". $address;?></h1>
							<?php foreach($abc as $new_cont_result){?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    <h5><?php echo $new_cont_result->firstName." ".$new_cont_result->lastName; ?></h5>
                                    <p><span><i class="fa fa-phone"></i></span><?php echo $new_cont_result->phoneNumber; ?></p>
                                    <p><span><i class="fa fa-phone"></i></span><?php echo $new_cont_result->homephoneNumber; ?></p>
                                    <p><span><i class="fa fa-envelope"></i></span><?php echo $new_cont_result->email; ?></p>
                                </div>
                            </div>
                            <?php } ?>    
                            <!--<h1><?php //echo $new_result[0]->displayName;?></h1>
                            <address>
                              <?php //if(!empty($address)) { echo $address; } ?>-->                                                                               <!--a href="#" class="map-link">
                                        <i class="fa fa-map-marker"></i> 
                                        <span class="ad-text">Directions</span>
                                    </a-->                                
                            <!--</address>-->
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-4">
						<?php 
										$statusType = $new_result[0]->objectStatus;
										$cType = $new_result[0]->contractType;
										if($cType == 'ENUMS_CONTRACTTYPES_TYPE_WRITE ASSIGNMENT')
										{
										  $contract_type = "WRITE ASSIGNMENT";
										}
										else if($cType == 'ENUMS_CONTRACTTYPES_TYPE_SALES')
										{
											$contract_type = "SALES";
										}
										else if($cType == 'ENUMS_CONTRACTTYPES_TYPE_PAID ESTIMATE')
										{
											$contract_type = "PAID ESTIMATE";
										}
										
										
										if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_IN_PROGRESS')
										{
										  $property_type = "In Progress";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_ESTIMATE')
										{
											$property_type = "ESTIMATE";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_DORMANT')
										{
											$property_type = "DORMANT";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_RESERVED')
										{
											$property_type = "RESERVED";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_COMING')
										{
											$property_type = "COMING";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_NO ASSIGNMENT')
										{
											$property_type = "NO ASSIGNMENT";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_LOST_LISTING')
										{
											$property_type = "";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_OTHER')
										{
											$property_type = "OTHER";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_ARCHIVED')
										{
											$property_type = "ARCHIVED";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_CLOSED')
										{
											$property_type = "CLOSED";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_REFERENCE')
										{
											$property_type = "REFERENCE";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_FOR_SALE')
										{
											$property_type = "FOR SALE";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_INTAKE')
										{
											$property_type = "INTAKE";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_MARKETING')
										{
											$property_type = "MARKETING";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_SOLD')
										{
											$property_type = "SOLD";
										}
										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_LEASED')
										{
											$property_type = "LEASED";
										}
										?>
                            <div class="pull-right ">
                                <div class="status-bar">
                                    <div class="asken-status">
                                        <a href="#" class="ask-status">Status: <?php echo $property_type;?></a>
                                    </div>
                                    <div class="asken-status">
                                        <a href="#" class="ask-status">Pris:  <?php if(!empty($startingPrice)) { echo "$".number_format($startingPrice); }?></a>
                                    </div>
                                </div>
                                <!--span class="price"> 
                                    <span class="price-goes-here"> <?php //if(!empty($startingPrice)) { echo "$".number_format($startingPrice); }?></span>
                                    <span class="currency">
                                    </span>
                                </span-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--section class="add-info-section">
            <div class="add-info-table">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="listing-add-info-list">
								<?php 
								//if($new_result[0]->isNewDevelopment==true)
								{?>	
								<ul class="add-list-info">
									<li><span class="number"><?php // echo $new_result[0]->minNumberOfRooms;?></span><br><span class="list-text">Bed</span></li>
									<li><span class="number"><?php //echo 'BathsFull';?></span><br><span class="list-text">Baths</span></li>
									<li><span class="number"><?php //echo $new_result[0]->maxLivingArea;?></span><br><span class="list-text">Sqft</span></li>
									<li><span class="number"><?php //echo $new_result[0]->builder;?></span><br><span class="list-text">Built</span></li>
								</ul>
								<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section-->
        <!--div class="fixed-container">
            <section class="bar-listing-tools">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="listing-tools-title">
                                <h3>Share Via: </h3>
								<div class="list-tool-list">
									<ul class="tools-list">
									 <?php //echo do_shortcode('[easy-social-share buttons="facebook,twitter,google,print" counters=0 style="button" point_type="simple"]'); ?> 	
									</ul>
								</div>
								<div class="button-holder">
									<a href="#" data-toggle="modal" data-target="#modal-request-info" class="button btn-request" modal-request-info>Request more info</a>
								</div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>
        </div-->
        <!--section class="open-house-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="open-house-box">
                            <a class="open-house1" style="cursor:pointer" id="bottom_form">
                                <h3 class="open-title"> <?php //echo $new_result[0]->displayName;?></h3>
                                <span class="time"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section-->
        <!--section class="section-overview">
            <div class="description-block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="full-description">
                                <div class="description-title">
                                    <h3>Contemporary Sophistication In Bronxville Village</h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="prop-desc">
                                            <p><?php echo $new_result[0]->description;?></p>
                                        </div>
                                        <div class="link-holder">
                                            <a href="#" class="link-more">Read More</a>
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>                                                                                                 
                </div>
            </div>
        </section-->
        <section class="detail-column-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-2">
                        <div class="detail-section-buttons">
                            <a href="#" class="btn btn-primary btn-design">Market</a>
                            <a href="#" class="btn btn-success btn-design">Display</a>
                            <a href="#" class="btn btn-info btn-design">Bidding</a>
                            <a href="#" class="btn btn-danger btn-design">Contract</a>
                            <a href="#" class="btn btn-primary btn-design">Access</a>
                            <a href="#" class="btn btn-success btn-design">Follow Up</a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9 col-md-10">
                        <div class="detail-menu-section">
                            <nav class="detail-menu">
                                <ul>
                                    <li class="active" data-toggle="tab" data-target="#Broker-Journal"><a href="#"><span><i class="fa fa-home"></i></span>Broker Journal</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Admin-Officer"><a href="#"><span><i class="fa fa-home"></i></span>Administrative Officer</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Mission"><a href="#"><span><i class="fa fa-home"></i></span>Mission</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Valuation"><a href="#"><span><i class="fa fa-home"></i></span>Valuation</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Seller"><a href="#"><i class="fa fa-home"></i></span>Seller</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Document"><a href="#"><span><i class="fa fa-home"></i></span>Document</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Filer"><a href="#"><i class="fa fa-home"></i></span>Filer</a>
                                    </li>
                                    <li data-toggle="tab" data-target="#Services"><a href="#"><i class="fa fa-home"></i></span>Services</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                        <div class="tab-content category-grid">
                            <div class="clearfix tab-pane detail-column-holder active" id="Broker-Journal">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <div class="panel-group accordion-width">
										
                                            <div class="panel panel-default">
                                                <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#broker-journal1" style="cursor:pointer;">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-plus-square"></i> 
                                                        Inform prospective buyers about how he intends to make the sale
                                                    </h4>
                                                </div>
                                                <div id="broker-journal1" class="panel-collapse collapse">
                                                    <div class="info">
                                                        <div class="col-xs-12">
                                                            <form>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed by</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#broker-journal2" style="cursor:pointer;">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-plus-square"></i> 
                                                        Inform prospective buyers about how he intends to make the sale
                                                    </h4>
                                                </div>
                                                <div id="broker-journal2" class="panel-collapse collapse">
                                                    <div class="info">
                                                        <div class="col-xs-12">
                                                            <form>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed by</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>                                                          
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#broker-journal3" style="cursor:pointer;">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-plus-square"></i> 
                                                        Inform prospective buyers about how he intends to make the sale
                                                    </h4>
                                                </div>
                                                <div id="broker-journal3" class="panel-collapse collapse">
                                                    <div class="info">
                                                        <div class="col-xs-12">
                                                            <form>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed by</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#broker-journal4" style="cursor:pointer;">
                                                    <h4 class="panel-title">
                                                        <i class="fa fa-plus-square"></i> 
                                                        Inform prospective buyers about how he intends to make the sale
                                                    </h4>
                                                </div>
                                                <div id="broker-journal4" class="panel-collapse collapse">
                                                    <div class="info">
                                                        <div class="col-xs-12">
                                                            <form>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-xs-12 col-sm-4">
                                                                                <label>The task performed by</label>
                                                                            </div>
                                                                            <div class="col-xs-12 col-sm-8">
                                                                                <p class="form-control-static">someone@example.com</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>                                                   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Admin-Officer">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <table class="table-fill">
                                            <thead>
                                                <tr>
                                                    <th class="text-left">Name</th>
                                                    <th class="text-left">Roll</th>
                                                    <th class="text-left">Responsible</th>
                                                    <th class="text-left">View online</th>
                                                    <th class="text-left">Activity</th>
                                                </tr>   
                                            </thead>
                                            <tbody class="table-hover">
                                                <tr>
                                                    <td class="text-left">Lotta hanson</td>
                                                    <td class="text-left">Realtors</td>
                                                    <td class="text-left">Yes</td>
                                                    <td class="text-left">YEs</td>
                                                    <td class="text-left">dfgtrg</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left">Lotta hanson</td>
                                                    <td class="text-left">Realtors</td>
                                                    <td class="text-left">Yes</td>
                                                    <td class="text-left">YEs</td>
                                                    <td class="text-left">dfgtrg</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Mission">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <div class="col-xs-12">
                                            <form style="width: 100%;">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed by</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed by</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>The task performed by</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Valuation">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <div class="col-xs-12">
                                            <form style="width: 100%;">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Price</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Living Space</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Latitude</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Number of Rooms </label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Length</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Street Address</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Zipcode</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-12 col-sm-4">
                                                                <label>Place</label>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-8">
                                                                <p class="form-control-static">someone@example.com</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>  
                                    </div>
                                </div>
                            </div>
<?php
/* $sellerdata = $wpdb->get_results("SELECT * FROM sellers where dealId = '".$id."'");

echo "<pre>";
print_r($sellerdata);
//echo "<pre>";
//print_r($broker_details); */

$broker_details = $wpdb->get_results("SELECT * FROM sellers where dealId = '".$id."'");
$primIdd = $broker_details[0]->contactId;

$sellerdata = $wpdb->get_results("SELECT * FROM contacts where id = '".$primIdd."'");
//echo "<pre>";
//print_r($sellerdata);
//echo "<pre>";
//print_r($broker_details);
?>
                            <div class="clearfix tab-pane detail-column-holder" id="Seller">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <table class="table-fill">
                                            <thead>
                                                <tr>
                                                    <th class="text-left">Name</th>
                                                    <th class="text-left">Telephone</th>
                                                    <th class="text-left">Email</th>
                                                    <th class="text-left">Address</th>
                                                </tr>   
                                            </thead>
                                            <tbody class="table-hover">
											<?php foreach($sellerdata as $sell_data){?>
                                                <tr>
                                                    <td class="text-left"><?php echo $sell_data->firstName." ".$sell_data->lastName; ?></td>
                                                    <td class="text-left"><?php echo $sell_data->phoneNumber; ?></td>
                                                    <td class="text-left"><?php echo $sell_data->email; ?></td>
                                                    <td class="text-left"><?php echo $sell_data->streetAddress.", ".$sell_data->postalCode.", ".$sell_data->city; ?></td>
                                                </tr>
											<?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Document">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <table class="table-fill">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Document Name</th>
                                                    <th class="text-center">Date</th>
                                                </tr>   
                                            </thead>
                                            <tbody class="table-hover">
                                                <tr>
                                                    <td class="text-center">Lotta hanson</td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Lotta hanson</td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Lotta hanson</td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Lotta hanson</td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">Lotta hanson</td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Filer">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <table class="table-fill">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">File Type</th>
                                                    <th class="text-center">Title</th>
                                                </tr>   
                                            </thead>
                                            <tbody class="table-hover">
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/png.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/pdf.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/png.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/png.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/pdf.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix tab-pane detail-column-holder" id="Services">
                                <div class="detail-column">
                                    <div class="accordion-section">
                                        <table class="table-fill">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">File Type</th>
                                                    <th class="text-center">Title</th>
                                                </tr>   
                                            </thead>
                                            <tbody class="table-hover">
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/bloom_logo.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/esoft-logo-white.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/bloom_logo.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/esoft-logo-white.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center"><img src="<?php bloginfo("template_directory"); ?>/assets/img/bloom_logo.png" alt=""></td>
                                                    <td class="text-center">Realtors</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                       
        </section>

        <!--section class="accordion-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                                    <div class="panel-group accordion-width">
                                        <div class="panel panel-default">
                                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#additional_info" style="cursor:pointer;">
                                                 <h4 class="panel-title"><i class="fa fa-plus-square"></i> Additional Information
                                                </h4>
                                            </div>
                                            <div id="additional_info" class="panel-collapse collapse">
                                                <div class="info">
            										<?php 
            										$statusType = $new_result[0]->objectStatus;
            										$cType = $new_result[0]->contractType;
            										if($cType == 'ENUMS_CONTRACTTYPES_TYPE_WRITE ASSIGNMENT')
            										{
            										  $contract_type = "WRITE ASSIGNMENT";
            										}
            										else if($cType == 'ENUMS_CONTRACTTYPES_TYPE_SALES')
            										{
            											$contract_type = "SALES";
            										}
            										else if($cType == 'ENUMS_CONTRACTTYPES_TYPE_PAID ESTIMATE')
            										{
            											$contract_type = "PAID ESTIMATE";
            										}
            										
            										
            										if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_IN_PROGRESS')
            										{
            										  $property_type = "In Progress";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_ESTIMATE')
            										{
            											$property_type = "ESTIMATE";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_DORMANT')
            										{
            											$property_type = "DORMANT";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_RESERVED')
            										{
            											$property_type = "RESERVED";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_COMING')
            										{
            											$property_type = "COMING";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_NO ASSIGNMENT')
            										{
            											$property_type = "NO ASSIGNMENT";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_LOST_LISTING')
            										{
            											$property_type = "";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_OTHER')
            										{
            											$property_type = "OTHER";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_ARCHIVED')
            										{
            											$property_type = "ARCHIVED";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_CLOSED')
            										{
            											$property_type = "CLOSED";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_REFERENCE')
            										{
            											$property_type = "REFERENCE";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_FOR_SALE')
            										{
            											$property_type = "FOR SALE";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_INTAKE')
            										{
            											$property_type = "INTAKE";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_MARKETING')
            										{
            											$property_type = "MARKETING";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_SOLD')
            										{
            											$property_type = "SOLD";
            										}
            										else if($statusType == 'ENUMS_OBJECTSTATUS_TYPE_LEASED')
            										{
            											$property_type = "LEASED";
            										}
            										?>
            										<ul class="list">
            										<li>Object status: <?php echo $property_type;?> </li>
            										<li>Contract type: <?php echo $contract_type;?></li>
            										<li>Listing start date: <?php echo $new_result[0]->listingStartDate;?></li>
            										<li>Listing end date: <?php echo $new_result[0]->listingEndDate;?></li>
            										<li>Asking price: <?php echo "$".number_format($new_result[0]->finalPrice);?></li>
            										<li>Asking price type: <?php echo $new_result[0]->startingPriceType;?></li>	
            										<li>Asking price in other currency: <?php echo $new_result[0]->foreignStartingPriceCurrencyId;?></li>
            										</ul>
            										
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading accordion-toggle collapsed" data-toggle="collapse" data-target="#amenities">
                                                 <h4 class="panel-title"><i class="fa fa-plus-square"></i>
                                                    <div>Broker & Seller Details</div>
                                              </h4>

                                            </div>
                                            <div id="amenities" class="panel-collapse collapse">
                                                <div class="panel-body">
            										<h3>Broker</h3>
                                                    <ul class="list">
            										<li>Name:  Johan  Siborne</li>
                                                    <li>Phone No: 0709-75 57 80</li>
                                                    <li>Email: johan@askengren.com</li> 
            										</ul>
            										<h3>Seller</h3>
            										<ul class="list">
                                                    <li>Name: Leif Rune Larsson </li>
                                                    <li>Phone No: 0709-75 57 80</li>
                                                    <li>Email: </li>
                                                </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    </div>
                </div>
            </div>
        </section-->


<!--div class="modal fade" id="modal-request-info" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request More Info</h4>
        </div>
        <div class="modal-body">
        <?php //echo do_shortcode('[contact-form-7 id="83" title="Contact form 1"]');?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div-->


<script>
	var scrolled = 0;
	$(document).ready(function () {
    $("#bottom_form").on("click", function () {
		//alert("here");
        scrolled = scrolled + 300;
        $(".contact_form").stop().animate({
			alert("here");
            scrollTop: scrolled
        });
    });

    $("#upClick").on("click", function () {
        scrolled = scrolled - 300;
        $(".cover").stop().animate({
            scrollTop: scrolled
        });
    });

    $(".clearValue").on("click", function () {
        scrolled = 0;
    });
});
</script>	
<?php get_footer();?>